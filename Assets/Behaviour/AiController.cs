﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.ObscuredTypes;

public class AiController : MonoBehaviour {

    // 현재 상태를 저장 할 delegate 형
    private delegate IEnumerator StateDelegator();
    // 현재 상태를 저장 할 delegate 멤버 변수
    private StateDelegator mStateDelegator = null;

    GameObject gameController;

    private Transform pTransform;
    private Animator anim;
    public Transform Blitzcrank;
    public Transform enemyBlitzcrank;

    public BlitzState PlayerState;

    public int PlayerNumber;

    [Range(5, 20)]
    public float rotateVelocity;
    public float clampAngle;

    // Temp Variable
    Quaternion tRot;
    float tRotZ;

    // About Flash
    [Range(1, 10)]
    public float flashDistance;
    public float flashCoolTime;
    private bool flashEnable;
    public Transform flashEffectPrefab;

    // About Grab
    public Transform GrabPointer;
    public SpriteRenderer GrabPointerSprite;
    private Transform GrabHand, GrabString;
    public float grabCoolTime;
    private bool grabEnable;

    // About Game
    public Transform shieldPrefab;
    public float RespawnTime, InvincibleTime;
    public float BuffDurationTime;
    public GameObject BlueEffect, RedEffect;
    private ObscuredFloat blueDuration, redDuration;

    public float BlueDuration { get { return blueDuration; } }
    public float RedDuration { get { return redDuration; } }

    int moveDir;

    public PlayerController player;

    private int AiDifficulty;

    //IsGamePlay
    private bool play;

    public void Initiate(Transform blitzSkin) {
        Transform blitz = Instantiate(blitzSkin, Vector3.zero, Quaternion.identity) as Transform;
        blitz.SetParent(Blitzcrank);
        blitz.localPosition = Vector3.zero;
        blitz.localRotation = Quaternion.identity;

        anim = blitz.GetComponent<Animator>();

        GrabString = blitz.FindChild("Grab_Arm_Right").FindChild("Grab_String");
        GrabHand = blitz.FindChild("Grab_Arm_Right").FindChild("Grab_String").FindChild("Grab_Hand");

        PlayerState = BlitzState.Respawn;
        mStateDelegator = Respawn;
        StartCoroutine(MainLoop());

        play = true;
        flashEnable = true; grabEnable = true;
    }

    // Use this for initialization
    void Start() {
        pTransform = transform;
        GrabPointer.gameObject.SetActive(false);
        moveDir = 0;
        PlayerState = BlitzState.None;
        flashEnable = false; grabEnable = false;
        play = false;
        AiDifficulty = 0;
        gameController = GameObject.FindWithTag("GameController");
    }

    // Update is called once per frame
    void Update() {
        if(play && PlayerState == BlitzState.Idle) {
            //Grab
            if(grabEnable) {
                RaycastHit2D hit = Physics2D.Raycast(Blitzcrank.position + GrabPointer.up.normalized * -1f, GrabPointer.up * -1f, 5f);
                if(hit.collider != null) {
                    //Player, Blue, Red 에 포인터가 위치할 때
                    if(hit.collider.tag == "Player" || hit.collider.tag == "Blue" || hit.collider.tag == "Rad") {
                        if(Random.Range(0f, 100f) < Mathf.Clamp(0.3f * AiDifficulty * AiDifficulty, 0.5f, 50f))
                            OnGrab();
                    }

                    //플레이어의 점멸거리에 포인터가 위치할 때
                    if(hit.collider.tag == "IfPlayerFlash" && player.PlayerState == BlitzState.Idle) {
                        if(Random.Range(0f, 100f) < Mathf.Clamp(0.05f * AiDifficulty * AiDifficulty - 10f, 0f, 10f)) {
                            StopCoroutine("ChangeAiDir");
                            StartCoroutine("ChangeAiDir", player.PlayerDir);
                            if(moveDir != 0)
                                OnFlash();
                            OnGrab();
                        }
                    }

                    //플레이어의 무빙 예측 지점에 포인터가 위치할 때
                    if(hit.collider.tag == "IfPlayerMove") {
                        if(player.PlayerState == BlitzState.Idle) {
                            if(Random.Range(0f, 100f) < 0.5f * AiDifficulty)
                                OnGrab();
                        }
                    }
                } else {
                    if(player.PlayerState == BlitzState.Idle) {
                        if(Random.Range(0f, 100f) < 0.3f - 0.03f * Mathf.Abs(AiDifficulty - 10))
                            OnGrab();
                    }
                }
            } // Check Grab End
        }
    }

    // 메인 루프
    IEnumerator MainLoop() {
        // 무한 반복 한다.
        while(true) {
            yield return null;

            // 현재 상태에 따라 해당 코루틴 함수를 실행 시킨다.
            yield return StartCoroutine(mStateDelegator());
        }
    }

    // Respawn Blitzcrank
    IEnumerator Respawn() {
        pTransform.rotation = Quaternion.identity;
        Blitzcrank.localPosition = new Vector3(0f, 13f, 0f);
        Blitzcrank.localRotation = Quaternion.identity;

        GrabPointer.gameObject.SetActive(true);
        StartCoroutine("TictocPointer", true);
        StartCoroutine(InvincibleUnit());

        anim.SetBool("Respawn", true);
        SoundManager.Instance.PlaySfx(Sfx.Respawn);

        blueDuration = 0f;
        redDuration = 0f;

        //Wait Respawn Animation
        yield return new WaitForSeconds(0.34f);

        //On End Animation
        anim.SetBool("Respawn", false);
        StartCoroutine("ChangeAiDir", Random.Range(-1, 2));
        PlayerState = BlitzState.Idle;
        mStateDelegator = Idle;
    }

    IEnumerator InvincibleUnit() {
        //invincible true
        Blitzcrank.GetComponent<CircleCollider2D>().enabled = false;
        Transform shield = Instantiate(shieldPrefab, Blitzcrank.position, Blitzcrank.rotation);
        shield.SetParent(Blitzcrank);

        yield return new WaitForSeconds(InvincibleTime);

        //invincible false
        Destroy(shield.gameObject);
        Blitzcrank.GetComponent<CircleCollider2D>().enabled = true;
    }

    // Idle (& Walking)
    IEnumerator Idle() {
        while(PlayerState == BlitzState.Idle) {
            MoveBlitz();
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator ChangeAiDir(int dir) {
        moveDir = dir;
        float moveDirPersistTime = GameConsts.StartMoveDirPersistTime - GameConsts.MoveDirPersistTimeReduceRate * (AiDifficulty / 3);
        yield return new WaitForSeconds(Mathf.Clamp(Random.Range(moveDirPersistTime - GameConsts.MoveDirPersistTimeReduceRate * 0.5f, moveDirPersistTime + GameConsts.MoveDirPersistTimeReduceRate * 0.5f),
            GameConsts.MinMoveDirPersistTime, GameConsts.MaxMoveDirPersistTime));
        int nextDir = dir;
        while(true) {
            if(pTransform.rotation.eulerAngles.z == clampAngle) {
                nextDir = -1;
                break;
            }
            if(pTransform.rotation.eulerAngles.z == 360 - clampAngle || pTransform.rotation.eulerAngles.z == - clampAngle) {
                nextDir = 1;
                break;
            }
            nextDir = Random.Range(-1, 2);
            if(dir != nextDir) {
                break;
            }
        }
        StartCoroutine("ChangeAiDir", nextDir);
    }

    void MoveBlitz() {
        tRot = pTransform.rotation;
        tRot *= Quaternion.AngleAxis(moveDir * rotateVelocity * Time.fixedDeltaTime * (redDuration != 0 ? GameConsts.RateOfBuffVelocity : 1f), Vector3.forward);

        tRotZ = Mathf.Clamp(tRot.eulerAngles.z > 180f ? tRot.eulerAngles.z - 360f : tRot.eulerAngles.z, clampAngle * -1f, clampAngle);

        pTransform.rotation = Quaternion.AngleAxis(tRotZ, Vector3.forward);

        Blitzcrank.localRotation = Quaternion.AngleAxis(moveDir * 50f, Vector3.back);

        if(moveDir != 0) {
            anim.SetBool("Walking", true);
        } else {
            anim.SetBool("Walking", false);
        }
    }

    public void OnFlash() {
        if(flashEnable && (PlayerState == BlitzState.Idle) && play) {
            Blitzcrank.GetComponent<CircleCollider2D>().enabled = false;

            SoundManager.Instance.PlaySfx(Sfx.Flash);

            Instantiate(flashEffectPrefab, Blitzcrank.position, Blitzcrank.rotation);
            StartCoroutine("CoolTime", "Flash");

            pTransform.rotation *= Quaternion.AngleAxis(moveDir * flashDistance, Vector3.forward);
            tRotZ = pTransform.eulerAngles.z;
            if(PlayerNumber == 1) {
                if(tRotZ > clampAngle && tRotZ < 180f)
                    pTransform.rotation = Quaternion.AngleAxis(clampAngle, Vector3.forward);
                if(tRotZ > 180f && tRotZ < 360f - clampAngle)
                    pTransform.rotation = Quaternion.AngleAxis(-clampAngle, Vector3.forward);
            }
            if(PlayerNumber == 2) {
                if(tRotZ > 180f + clampAngle)
                    pTransform.rotation = Quaternion.AngleAxis(180f + clampAngle, Vector3.forward);
                if(tRotZ < 180f - clampAngle)
                    pTransform.rotation = Quaternion.AngleAxis(180f - clampAngle, Vector3.forward);
            }

            Blitzcrank.localRotation = Quaternion.AngleAxis(moveDir * 50f, Vector3.back);

            Blitzcrank.GetComponent<CircleCollider2D>().enabled = true;
        }
    }

    public void OnGrab() {
        if(grabEnable && (PlayerState == BlitzState.Idle)) {
            PlayerState = BlitzState.Grab;
            mStateDelegator = Grab;
            StartCoroutine("CoolTime", "Grab");
        }
    }

    IEnumerator CoolTime(string name) {
        float cTimer;
        switch(name) {
        case "Flash":
            flashEnable = false;
            cTimer = flashCoolTime;
            while(cTimer > 0) {
                cTimer -= (Time.fixedDeltaTime) * (blueDuration != 0f ? GameConsts.RateOfBuffCoolTime : 1f);
                yield return new WaitForFixedUpdate();
            }
            flashEnable = true;
            break;

        case "Grab":
            grabEnable = false;
            GrabPointerSprite.color = Color.black;
            cTimer = grabCoolTime;
            while(cTimer > 0) {
                cTimer -= (Time.fixedDeltaTime) * (blueDuration != 0f ? GameConsts.RateOfBuffCoolTime : 1f);
                yield return new WaitForFixedUpdate();
            }
            GrabPointerSprite.color = new Color32(0, 255, 234, 255);
            grabEnable = true;
            break;
        }
    }

    public void GrabObject(Transform obj) {
        switch(obj.tag) {
        case "Player":
            if(obj != Blitzcrank && PlayerState == BlitzState.Grab) {
                if(player.IgnoreGrab) {
                    player.DefenseGrab();
                } else {
                    SoundManager.Instance.PlaySfx(Sfx.Catch);
                    enemyBlitzcrank = obj;
                    PlayerState = BlitzState.Pulling;
                    mStateDelegator = Pulling;
                }
            }
            break;

        case "Blue":
            SoundManager.Instance.PlaySfx(Sfx.Catch);
            gameController.SendMessage("GetBuff", "Blue");
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            anim.SetBool("Grab", false);
            //GetBlue
            blueDuration = BuffDurationTime;
            StartCoroutine("GetBuff", "Blue");
            Destroy(obj.parent.gameObject);
            break;

        case "Red":
            SoundManager.Instance.PlaySfx(Sfx.Catch);
            gameController.SendMessage("GetBuff", "Red");
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            anim.SetBool("Grab", false);
            //GetRed
            redDuration = BuffDurationTime;
            StartCoroutine("GetBuff", "Red");
            Destroy(obj.parent.gameObject);
            break;

        case "Minion":
            SoundManager.Instance.PlaySfx(Sfx.Catch);
            GrabHand.GetComponent<CircleCollider2D>().enabled = false;
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            GrabHand.GetComponent<CircleCollider2D>().enabled = true;
            anim.SetBool("Grab", false);
            //Add Gold
            gameController.SendMessage("GetMinionByCom");
            Destroy(obj.parent.gameObject);
            break;
        }
    }

    void OnStruggle() {
        anim.SetBool("Grab", false);
        PlayerState = BlitzState.Struggle;
        mStateDelegator = Struggle;
        StopCoroutine("ChangeAiDir");
    }

    IEnumerator Struggle() {
        GrabPointer.gameObject.SetActive(false);
        StopCoroutine("TictocPointer");
        anim.SetBool("Struggle", true);
        Blitzcrank.localRotation = Quaternion.identity;
        Blitzcrank.GetComponent<CircleCollider2D>().enabled = false;

        while(PlayerState == BlitzState.Struggle)
            yield return new WaitForFixedUpdate();
    }

    void OnDead() {
        StopCoroutine("GetBuff");
        RedEffect.SetActive(false);
        BlueEffect.SetActive(false);
        redDuration = 0f;
        blueDuration = 0f;

        AiDifficulty++;

        PlayerState = BlitzState.Dead;
        mStateDelegator = Dead;
    }

    IEnumerator Dead() {
        anim.SetBool("Struggle", false);
        anim.SetBool("Dead", true);
        yield return new WaitForSeconds(RespawnTime);
        anim.SetBool("Dead", false);
        PlayerState = BlitzState.Respawn;
        mStateDelegator = Respawn;
    }

    IEnumerator TictocPointer(bool clockwise) {
        int frameCnt = 30;
        float rotateAngle = 80f;
        if(clockwise)
            GrabPointer.rotation = Quaternion.AngleAxis(rotateAngle / 2f, Vector3.forward);
        else
            GrabPointer.rotation = Quaternion.AngleAxis(rotateAngle / 2f, Vector3.back);

        for(int i = 0; i < frameCnt; i++) {
            if(clockwise) {
                GrabPointer.rotation = Quaternion.AngleAxis((rotateAngle / 2f) - i * rotateAngle / (float)frameCnt, Vector3.forward);
                yield return new WaitForFixedUpdate();
            } else {
                GrabPointer.rotation = Quaternion.AngleAxis((rotateAngle / 2f) - i * rotateAngle / (float)frameCnt, Vector3.back);
                yield return new WaitForFixedUpdate();
            }
        }

        StartCoroutine("TictocPointer", !clockwise);
    }

    IEnumerator Grab() {
        anim.SetBool("Grab", true);
        SoundManager.Instance.PlaySfx(Sfx.LaunchGrab);
        Blitzcrank.rotation = GrabPointer.rotation;

        if(Blitzcrank.GetChild(2).name.Contains("Police"))
            SoundManager.Instance.PlaySfx(Sfx.Siren);

        int frameCnt = 20;
        GrabString.localScale = new Vector3(1f, 0f, 1f);
        GrabHand.localScale = Vector3.one;
        for(int i = 0; i < frameCnt && PlayerState == BlitzState.Grab; i++) {
            GrabString.localScale = new Vector3(1f, 1.2f * (i + 1) / (float)frameCnt, 1f);
            GrabHand.localScale = new Vector3(1f, 1f / (1.2f * (i + 1) / (float)frameCnt), 1f);
            yield return new WaitForFixedUpdate();
        }

        if(PlayerState != BlitzState.Pulling && PlayerState != BlitzState.Struggle) {
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            anim.SetBool("Grab", false);
        }
    }

    IEnumerator Pulling() {
        GrabHand.GetComponent<CircleCollider2D>().enabled = false;
        enemyBlitzcrank.SendMessageUpwards("OnStruggle");

        gameController.SendMessage("GetScore", PlayerNumber);

        float grabStartPoint = GrabString.localScale.y; int frameCnt = (int)(20f * (grabStartPoint - 0.05f) / 1.2f);
        for(int i = 0; i < frameCnt; i++) {
            GrabString.localScale = new Vector3(1f, grabStartPoint - (1.2f * (i + 1) / 20f), 1f);
            GrabHand.localScale = new Vector3(1f, 1f / (grabStartPoint - 1.2f * (i + 1) / 20f), 1f);
            enemyBlitzcrank.position = GrabHand.position;
            yield return new WaitForFixedUpdate();
        }

        if(player.BlueDuration != 0) {
            blueDuration = player.BlueDuration;
            StartCoroutine("GetBuff", "Blue");
        }
        if(player.RedDuration != 0) {
            redDuration = player.RedDuration;
            StartCoroutine("GetBuff", "Red");
        }

        enemyBlitzcrank.SendMessageUpwards("OnDead");
        GrabHand.GetComponent<CircleCollider2D>().enabled = true;

        anim.SetBool("Grab", false);
        PlayerState = BlitzState.Idle;
        mStateDelegator = Idle;
    }

    IEnumerator GetBuff(string buff) {
        switch(buff) {
        case "Blue":
            BlueEffect.SetActive(true);
            while(blueDuration > 0) {
                blueDuration -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
            blueDuration = 0f;
            BlueEffect.SetActive(false);
            break;

        case "Red":
            RedEffect.SetActive(true);
            while(redDuration > 0) {
                redDuration -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
            redDuration = 0f;
            RedEffect.SetActive(false);
            break;
        }
    }

    public void OnPlayerGrab(Vector3 origin, Vector3 direction) {
        RaycastHit2D[] hits = Physics2D.RaycastAll(origin, direction);
        foreach(RaycastHit2D hit in hits) {
            if(hit.transform == Blitzcrank) {
                if(Random.Range(0f, 100f) < 0.5f * AiDifficulty * AiDifficulty) {
                    StopCoroutine("ChangeAiDir");
                    if(pTransform.rotation.eulerAngles.z <= clampAngle) {
                        StartCoroutine("ChangeAiDir", -1);
                    } else {
                        StartCoroutine("ChangeAiDir", 1);
                    }
                    if(Random.Range(0f, 100f) < 0.2f * AiDifficulty * AiDifficulty)
                        OnFlash();
                }
            }
        }
    }

    public void OnGameEnd() {
        play = false;
    }

    public void OnContinue() {
        PlayerState = BlitzState.Respawn;
        mStateDelegator = Respawn;

        play = true;
    }
}
