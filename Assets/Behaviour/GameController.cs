﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.ObscuredTypes;

public class GameController : MonoBehaviour {
    protected bool play;

    //blitz skins
    public Transform[] Blitzcranks;

    //Play Timer
    protected ObscuredFloat gameTimer;

    //Buff
    public float buffGenInterval;
    protected float blueTimer, redTimer, minionTimer;
    protected bool blueAlive, redAlive, minionAlive;

    //Mob Prefab
    public Transform redPrefab, bluePrefab;
    public Transform[] minionPrefabs;

    // For Annouce
    protected int recentKiller = -1, killCount = 0;

    public class GoldEffectInfo {
        public int Gold;
        public Vector3 EffectPos;

        public GoldEffectInfo(int gold, Vector3 position) {
            this.Gold = gold;
            this.EffectPos = position;
        }
    }

    // Use this for initialization
    void Start () {
       
    }

    // Update is called once per frame
    void Update () {
       
    }

    protected void GenerateMinion(int minionType) {
        StartCoroutine(WaitMinionGenerate(minionType));
    }

    protected void GenerateBlue() {
        Transform blue = Instantiate(bluePrefab, new Vector3(-5.2f, 0f, 0f), Quaternion.AngleAxis(90f, Vector3.forward)) as Transform;
    }

    protected void GenerateRed() {
        Transform red = Instantiate(redPrefab, new Vector3(5.2f, 0f, 0f), Quaternion.AngleAxis(270f, Vector3.forward)) as Transform;
    }

    protected void GetBuff(string buff) {
        switch(buff) {
        case "Blue":
            blueAlive = false;
            break;

        case "Red":
            redAlive = false;
            break;
        }
    }

    public void BackToMain() {
        SoundManager.Instance.PlaySfx(Sfx.Button);
        SceneManager.LoadScene("Main");
    }

    IEnumerator WaitMinionGenerate(int minionType) {
        Transform minion = Instantiate(minionPrefabs[minionType], new Vector3(Random.Range(-3f, 3f), Random.Range(-0.5f, 0.5f), 0f), Quaternion.AngleAxis(Random.Range(0, 2) * 180f, Vector3.forward)) as Transform;
        Animation minionAnim = minion.GetComponent<Animation>();
        while(minionAnim.isPlaying) {
            yield return new WaitForFixedUpdate();
        }
        minionAnim.Play("MinionIdle");
    }

    protected virtual void GetScore(int playerNumber) { }

    protected virtual void GameStart() { }
    protected virtual void OnGameEnd() { }
}
