﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using UnityEngine.Advertisements;

public class PVAGameController : GameController {
    //Secure
    private bool cheaterDetected = false;

    //Ready UI
    public Image playerSkin;
    public Sprite[] skins;

    public GameObject ReadyUI;
    public GameObject GameUI;
    public GameObject ResultUI;
    public GameObject PauseUI;
    public GameObject RateUsUI;
    public GameObject Btns;

    //Players
    public PlayerController player; //Player Number is 2
    public AiController com; // Player Nubmer is 0

    //About Game
    [Range(10, 120)]
    public float PVAGameTime;

    private ObscuredInt comScore, playerScore;
    private ObscuredInt gold, bonusGold;
    private ObscuredInt minionCount;

    //GameUI
    public Text PlayerScoreText, TimerText;
    public Text TimerText_10sec;
    
    //ResultUI
    public Text ResultScoreText;
    public Text ResultGoldText;
    public Image ResultHighScore;

    //RateUsUI
    public Image RateUsWindow;
    public Image RateYes, RateNo;
    public Sprite[] RateWindowImg, RateYesImg, RateNoImg; // 0 -> KR, 1-> EN

    //Effect
    public Transform goldEffect;

    string[] skinUnlock = GameManager.Instance.SkinUnlock;

    //Unity Ads
    public Button BtnAds;
    private bool canContinue;
    ShowOptions ShowOpt = new ShowOptions();

    private void OnCheaterDetected() {
        cheaterDetected = true;
    }

    void Awake() {
        ShowOpt.resultCallback = OnAdsShowResultCallBack;
        Vungle.onAdFinishedEvent += FinishedViewAds;
    }

    // Use this for initialization
    void Start() {
        ObscuredCheatingDetector.StartDetection(OnCheaterDetected);

        play = false;

        // Set UI
        GameUI.SetActive(false);
        ResultUI.SetActive(false);
        PauseUI.SetActive(false);
        ReadyUI.SetActive(true);
        RateUsUI.SetActive(false);

        //Set Default Player Skin
        playerSkin.sprite = skins[(int)GameManager.Instance.PlayerSkin];

        // Set Timer
        blueTimer = buffGenInterval;
        redTimer = buffGenInterval;
        gameTimer = PVAGameTime + GameConsts.TotalBonus.BonusPlayTime;

        gold = 0; bonusGold = 0;
        playerScore = 0;
        minionCount = 0;
        minionTimer = Random.Range(5f, 7f);
        PlayerScoreText.text = "0";

        Time.timeScale = 1f;

        canContinue = true;

        // Localization
        switch(GameManager.Instance.GameLanguage) {
        case "Korean":
            RateUsWindow.sprite = RateWindowImg[0];
            RateYes.sprite = RateYesImg[0];
            RateNo.sprite = RateNoImg[0];
            break;

        default:
            RateUsWindow.sprite = RateWindowImg[1];
            RateYes.sprite = RateYesImg[1];
            RateNo.sprite = RateNoImg[1];
            break;
        }
    }

    public new void BackToMain() {
        Vungle.onAdFinishedEvent -= FinishedViewAds;
        SoundManager.Instance.PlaySfx(Sfx.Button);
        SceneManager.LoadScene("Main");
    }

    void ContinueForRewards() {
        play = true;
        gameTimer += GameConsts.AddTime;
        GameManager.Instance.UseGold(gold);
        GameManager.Instance.UseGold(bonusGold);
        ResultUI.SetActive(false);
        GameUI.SetActive(true);
        Time.timeScale = 1f;
        canContinue = false;
        com.OnContinue();
        player.OnContinue();
    }

    void OnAdsShowResultCallBack(ShowResult result) {
        if(result == ShowResult.Finished) {
            ContinueForRewards();
        }
    }

    public void FinishedViewAds(AdFinishedEventArgs args) {
        if(args.IsCompletedView) {
            ContinueForRewards();
        }
    }

    // Update is called once per frame
    void Update() {
        if(cheaterDetected) {
            Application.Quit();
        }

        if(play) {
            gameTimer -= Time.deltaTime;
            if(!minionAlive) {
                minionTimer -= Time.deltaTime;
            }
            if(!blueAlive) {
                blueTimer -= Time.deltaTime;
            }
            if(!redAlive) {
                redTimer -= Time.deltaTime;
            }

            if(minionTimer < 0) {
                GenerateMinion(Mathf.Clamp(minionCount / 3, 0, 2));
                minionTimer = Random.Range(5f, 7f);
                minionAlive = true;
            }

            if(blueTimer < 0) {
                GenerateBlue();
                blueTimer = buffGenInterval;
                blueAlive = true;
            }

            if(redTimer < 0) {
                GenerateRed();
                redTimer = buffGenInterval;
                redAlive = true;
            }

            if(gameTimer <= 0) {
                TimerText_10sec.text = "0.0";
                OnGameEnd();
            } else if(gameTimer <= 10) {
                TimerText_10sec.text = gameTimer.ToString("0.0");
                TimerText_10sec.color = new Color(1f, 0.1f * gameTimer, 0.1f * gameTimer, 0.5f);
            } else {
                TimerText_10sec.text = "";
            }

            TimerText.text = Mathf.Clamp(gameTimer, 0f, 99999f).ToString("0");

            if(Input.GetKeyDown(KeyCode.Escape)) {
                //Pause
                if(PauseUI.activeInHierarchy) {
                    OnResume();
                } else {
                    Time.timeScale = 0f;
                    PauseUI.SetActive(true);
                }
            }
        } else {
            if(Input.GetKeyDown(KeyCode.Escape) && (ResultUI.activeInHierarchy|| ReadyUI.activeInHierarchy)) {
                BackToMain();
            }
        }
    }

    public void OnButtonAds() {
        if(Advertisement.IsReady() && GameManager.Instance.VideoAdsOrder == 0) {
            Advertisement.Show(ShowOpt);
            GameManager.Instance.VideoAdsOrder = 1;
        } else {
            Dictionary<string, object> options = new Dictionary<string, object>();
            options["incentivized"] = true;
            options["orientation"] = VungleAdOrientation.AutoRotate;
            Vungle.playAdWithOptions(options);
            GameManager.Instance.VideoAdsOrder = 0;
        }
    }

    public void OnResume() {
        Time.timeScale = 1f;
        PauseUI.SetActive(false);
    }

    public void ChangePlayerSkin(string direction) {
        SoundManager.Instance.PlaySfx(Sfx.Button);
        int skinNumber = (int)GameManager.Instance.PlayerSkin;
        switch(direction) {
        case "previous":
            skinNumber--;
            while(true) {
                if(skinNumber >= 0) {
                    if(skinUnlock[skinNumber] == "True") {
                        break;
                    } else {
                        skinNumber--;
                    }
                } else {
                    skinNumber = GameConsts.Skins.Length - 1;
                }
            }
            GameManager.Instance.SetPlayerSkin(0, (SkinList)(skinNumber));
            break;

        case "next":
            skinNumber++;
            while(true) {
                if(skinNumber < GameConsts.Skins.Length) {
                    if(skinUnlock[skinNumber] == "True") {
                        break;
                    } else {
                        skinNumber++;
                    }
                } else {
                    skinNumber = 0;
                }
            }
            GameManager.Instance.SetPlayerSkin(0, (SkinList)(skinNumber));
            break;
        }
        playerSkin.sprite = skins[(int)GameManager.Instance.PlayerSkin];
    }

    public void SelectSkin() {
        SoundManager.Instance.PlaySfx(Sfx.Button);
        Btns.SetActive(false);
        GameStart();
    }

    protected override void GameStart() {
        GameUI.SetActive(true);
        ReadyUI.SetActive(false);

        player.Initiate(Blitzcranks[(int)GameManager.Instance.PlayerSkin]);
        com.Initiate(Blitzcranks[Random.Range(0, GameConsts.Skins.Length)]);

        SoundManager.Instance.Announce(AnnounceScript.Welcome);

        play = true;
    }

    protected void GetMinion(GoldEffectInfo goldInfo) {
        minionAlive = false;
        goldInfo.Gold = GameConsts.MinionGold[Mathf.Clamp(minionCount / 3, 0, 2)];
        GetGold(goldInfo);
        minionCount++;
    }

    private void GetMinionByCom() {
        minionAlive = false;
    }

    void GetGold(GoldEffectInfo goldInfo) {
        gold += goldInfo.Gold;
        Instantiate(goldEffect, goldInfo.EffectPos, Quaternion.identity).GetComponent<GoldTextEffect>().ShowGoldEffect(goldInfo.Gold);
    }

    IEnumerator GetGoldWithDelayForKill(int gold) {
        this.gold += gold;
        yield return new WaitForSeconds(0.3f);
        Instantiate(goldEffect, com.Blitzcrank.position, Quaternion.identity).GetComponent<GoldTextEffect>().ShowGoldEffect(gold);
    }

    protected override void OnGameEnd() {
        StartCoroutine(DelayResult());
    }

    IEnumerator DelayResult() {
        //Player play false
        play = false;
        player.OnGameEnd();
        com.OnGameEnd();
        SoundManager.Instance.PlaySfx(Sfx.GameOver);

        yield return new WaitForSeconds(1f);

        // Get Gold
        bonusGold = Mathf.RoundToInt(gold * GameConsts.TotalBonus.BonusGold * 0.01f);

        GameManager.Instance.AddGold(gold);
        GameManager.Instance.AddGold(bonusGold);

        //SetUI
        GameUI.SetActive(false);

        ResultScoreText.text = playerScore.ToString();
        ResultGoldText.text = (gold + bonusGold).ToString();


        if(playerScore > GameManager.Instance.HighScore) {
            GameManager.Instance.SetHighScore(playerScore);
            ResultHighScore.enabled = true;
        }

        GameManager.Instance.UnlockScoreAchievement();

        BtnAds.interactable = canContinue;
        ResultUI.SetActive(true);
        yield return new WaitForSeconds(1f);
        Time.timeScale = 0f;

        ResultUI.GetComponent<Animator>().Play("DrawResultCardDown");
        GameManager.Instance.ShowInterstitial();
    }

    public void OnRestart() {
        if(ResultHighScore.enabled && playerScore >= 10 && !RateUsUI.activeInHierarchy) {
            if(PlayerPrefs.GetString("AlreadyRate", "False") == "False") {
                RateUsUI.SetActive(true);
                return;
            }
        }
        Vungle.onAdFinishedEvent -= FinishedViewAds;
        SceneManager.LoadScene("PlayerVsComputer");
    }

    public void RateUs(bool yes) {
        if(yes) {
            Application.OpenURL(GameConsts.RatingURL);
            PlayerPrefs.SetString("AlreadyRate", "True");
        }
        RateUsUI.SetActive(false);
    }

    protected override void GetScore(int playerNumber) {
        if(recentKiller == playerNumber) {
            killCount++;
            switch(killCount) {
            case 2:
                SoundManager.Instance.Announce(AnnounceScript.DoubleKill);
                break;
            case 3:
                SoundManager.Instance.Announce(AnnounceScript.TripleKill);
                break;
            case 4:
                SoundManager.Instance.Announce(AnnounceScript.QuadraKill);
                break;
            case 5:
                SoundManager.Instance.Announce(AnnounceScript.PentaKill);
                break;
            case 6:
                SoundManager.Instance.Announce(AnnounceScript.KillingSpree);
                break;
            case 7:
                SoundManager.Instance.Announce(AnnounceScript.Rampage);
                break;
            case 8:
                SoundManager.Instance.Announce(AnnounceScript.Unstoppable);
                break;
            case 9:
                SoundManager.Instance.Announce(AnnounceScript.Dominating);
                break;
            case 10:
                SoundManager.Instance.Announce(AnnounceScript.Godlike);
                break;
            default:
                SoundManager.Instance.Announce(AnnounceScript.Legendary);
                break;
            }
        } else {
            if((playerScore + comScore) == 0) {
                SoundManager.Instance.Announce(AnnounceScript.FirstBlood);
            } else {
                if(killCount >= 3) {
                    SoundManager.Instance.Announce(AnnounceScript.Shutdown);
                } else {
                    if(playerNumber == 0) {
                        SoundManager.Instance.Announce(AnnounceScript.KilledByEnemy);
                    } else {
                        SoundManager.Instance.Announce(AnnounceScript.KillEnemy);
                    }
                }
            }
            recentKiller = playerNumber;
            killCount = 1;
        }

        if(playerNumber == 0) {
            comScore++;
        }

        if(playerNumber == 2) {
            StartCoroutine(GetGoldWithDelayForKill((playerScore + 1) * GameConsts.BlitzKillGold));
            playerScore++;
            gameTimer += GameConsts.PlusTime;
            PlayerScoreText.text = playerScore.ToString();
        }
    }
}
