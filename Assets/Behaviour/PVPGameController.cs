﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PVPGameController : GameController {
    //PVP Ready UI
    public Image rPlayer1Skin, rPlayer2Skin;
    public Sprite[] skins;

    public GameObject ReadyUI;
    public GameObject GameUI;
    public GameObject ResultUI;
    public GameObject P1SBtns, P2SBtns;

    //PVP GameUI
    public Text P1ScoreText, P2ScoreText;
    public Text TimerText1, TimerText2;
    public Text TimerText_10sec_1, TimerText_10sec_2;
    public GameObject PauseUI;

    //PVP ResultUI
    public Text ResultScore;
    public Image RedWin, BlueWin, Draw1, Draw2;
    public Image ResultScreen, ResultDecoBar;

    //Players
    public PlayerController player1, player2;

    //About Game
    [Range(10, 120)]
    public float PVPGameTime;
    
    //PVP Score
    private int p1Score, p2Score;

    string[] skinUnlock = GameManager.Instance.SkinUnlock;

    // Use this for initialization
    void Start() {
        play = false;
        blueTimer = buffGenInterval;
        redTimer = buffGenInterval;
        GameUI.SetActive(false);
        ResultUI.SetActive(false);
        ReadyUI.SetActive(true);
        rPlayer1Skin.sprite = skins[(int)GameManager.Instance.Player1Skin];
        rPlayer2Skin.sprite = skins[(int)GameManager.Instance.Player2Skin];
        P1ScoreText.text = "0";
        P2ScoreText.text = "0";
        gameTimer = PVPGameTime;
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update() {
        if(play) {
            gameTimer -= Time.deltaTime;

            if(!blueAlive) {
                blueTimer -= Time.deltaTime;
            }
            if(!redAlive) {
                redTimer -= Time.deltaTime;
            }

            if(blueTimer < 0) {
                GenerateBlue();
                blueTimer = buffGenInterval;
                blueAlive = true;
            }

            if(redTimer < 0) {
                GenerateRed();
                redTimer = buffGenInterval;
                redAlive = true;
            }

            if(gameTimer <= 0) {
                TimerText_10sec_1.text = "0.0";
                TimerText_10sec_2.text = "0.0";
                OnGameEnd();
            } else if(gameTimer <= 10) {
                TimerText_10sec_1.text = gameTimer.ToString("0.0");
                TimerText_10sec_1.color = new Color(1f, 0.1f * gameTimer, 0.1f * gameTimer, 0.35f);
                TimerText_10sec_2.text = gameTimer.ToString("0.0");
                TimerText_10sec_2.color = new Color(1f, 0.1f * gameTimer, 0.1f * gameTimer, 0.35f);
            } else {
                TimerText_10sec_1.text = "";
                TimerText_10sec_2.text = "";
            }

            TimerText1.text = Mathf.Clamp(gameTimer, 0f, 99999f).ToString("0");
            TimerText2.text = Mathf.Clamp(gameTimer, 0f, 99999f).ToString("0");

            if(Input.GetKeyDown(KeyCode.Escape)) {
                //Pause
                if(PauseUI.activeInHierarchy) {
                    OnResume();
                } else {
                    Time.timeScale = 0f;
                    PauseUI.SetActive(true);
                }
            }
        } else {
            if(Input.GetKeyDown(KeyCode.Escape) && ResultUI.activeInHierarchy) {
                BackToMain();
            }
        }
    }

    public void OnResume() {
        Time.timeScale = 1f;
        PauseUI.SetActive(false);
    }

    public void ChangeP1Skin(string direction) {
        SoundManager.Instance.PlaySfx(Sfx.Button);
        int skinNumber = (int)GameManager.Instance.Player1Skin;
        switch(direction) {
        case "previous":
            skinNumber--;
            while(true) {
                if(skinNumber >= 0) {
                    if(skinUnlock[skinNumber] == "True") {
                        break;
                    } else {
                        skinNumber--;
                    }
                } else {
                    skinNumber = GameConsts.Skins.Length - 1;
                }
            }
            GameManager.Instance.SetPlayerSkin(1, (SkinList)(skinNumber));
            break;

        case "next":
            skinNumber++;
            while(true) {
                if(skinNumber < GameConsts.Skins.Length) {
                    if(skinUnlock[skinNumber] == "True") {
                        break;
                    } else {
                        skinNumber++;
                    }
                } else {
                    skinNumber = 0;
                }
            }
            GameManager.Instance.SetPlayerSkin(1, (SkinList)(skinNumber));
            break;
        }
        rPlayer1Skin.sprite = skins[(int)GameManager.Instance.Player1Skin];
    }

    public void ChangeP2Skin(string direction) {
        SoundManager.Instance.PlaySfx(Sfx.Button);
        int skinNumber = (int)GameManager.Instance.Player2Skin;
        switch(direction) {
        case "previous":
            skinNumber--;
            while(true) {
                if(skinNumber >= 0) {
                    if(skinUnlock[skinNumber] == "True") {
                        break;
                    } else {
                        skinNumber--;
                    }
                } else {
                    skinNumber = GameConsts.Skins.Length - 1;
                }
            }
            GameManager.Instance.SetPlayerSkin(2, (SkinList)(skinNumber));
            break;

        case "next":
            skinNumber++;
            while(true) {
                if(skinNumber < GameConsts.Skins.Length) {
                    if(skinUnlock[skinNumber] == "True") {
                        break;
                    } else {
                        skinNumber++;
                    }
                } else {
                    skinNumber = 0;
                }
            }
            GameManager.Instance.SetPlayerSkin(2, (SkinList)(skinNumber));
            break;
        }
        rPlayer2Skin.sprite = skins[(int)GameManager.Instance.Player2Skin];
    }

    public void SelectSkin(int playerNumber) {
        SoundManager.Instance.PlaySfx(Sfx.Button);
        if(playerNumber == 1) {
            P1SBtns.SetActive(false);
        }
        if(playerNumber == 2) {
            P2SBtns.SetActive(false);
        }
        if(!P1SBtns.activeInHierarchy && !P2SBtns.activeInHierarchy) {
            GameStart();
        }
    }

    protected override void GameStart() {
        GameUI.SetActive(true);
        ReadyUI.SetActive(false);

        player1.Initiate(Blitzcranks[(int)GameManager.Instance.Player1Skin]);
        player2.Initiate(Blitzcranks[(int)GameManager.Instance.Player2Skin]);

        SoundManager.Instance.Announce(AnnounceScript.Welcome);

        play = true;
    }
   
    protected override void OnGameEnd() {
        StartCoroutine(DelayResult());
    }

    IEnumerator DelayResult() {
        //Player play false
        play = false;
        GameUI.SetActive(false);
        player1.OnGameEnd();
        player2.OnGameEnd();
        SoundManager.Instance.PlaySfx(Sfx.GameOver);

        yield return new WaitForSeconds(1f);

        int winner = 0;
        if(p1Score > p2Score) {
            winner = 1;
            ResultDecoBar.color = Color.red;
            RedWin.enabled = true;
        } else if(p1Score == p2Score) {
            winner = 0;
            ResultDecoBar.color = Color.green;
            Draw1.enabled = true;
            Draw2.enabled = true;
        } else {
            winner = 2;
            ResultDecoBar.color = Color.blue;
            BlueWin.enabled = true;
        }

        ResultScreen.rectTransform.rotation = Quaternion.AngleAxis(winner == 1 ? 180f : 0f, Vector3.forward);
        ResultScore.text = p1Score.ToString() + "  :  " + p2Score.ToString();
        ResultUI.SetActive(true);
        Time.timeScale = 0f;

        GameManager.Instance.ShowInterstitial();
    }

    public void OnRestart() {
        SceneManager.LoadScene("PlayerVSPlayer");
    }

    protected override void GetScore(int playerNumber) {
        if(recentKiller == playerNumber) {
            killCount++;
            switch(killCount) {
            case 2:
                SoundManager.Instance.Announce(AnnounceScript.DoubleKill);
                break;
            case 3:
                SoundManager.Instance.Announce(AnnounceScript.TripleKill);
                break;
            case 4:
                SoundManager.Instance.Announce(AnnounceScript.QuadraKill);
                break;
            case 5:
                SoundManager.Instance.Announce(AnnounceScript.PentaKill);
                break;
            case 6:
                SoundManager.Instance.Announce(AnnounceScript.KillingSpree);
                break;
            case 7:
                SoundManager.Instance.Announce(AnnounceScript.Rampage);
                break;
            case 8:
                SoundManager.Instance.Announce(AnnounceScript.Unstoppable);
                break;
            case 9:
                SoundManager.Instance.Announce(AnnounceScript.Dominating);
                break;
            case 10:
                SoundManager.Instance.Announce(AnnounceScript.Godlike);
                break;
            default:
                SoundManager.Instance.Announce(AnnounceScript.Legendary);
                break;
            }
        } else {
            if((p1Score + p2Score) == 0) {
                SoundManager.Instance.Announce(AnnounceScript.FirstBlood);
            } else {
                if(killCount >= 3) {
                    SoundManager.Instance.Announce(AnnounceScript.Shutdown);
                } else {
                    SoundManager.Instance.Announce(AnnounceScript.KillEnemy);
                }
            }
            recentKiller = playerNumber;
            killCount = 1;
        }

        if(playerNumber == 1) {
            p1Score++;
            P1ScoreText.text = p1Score.ToString();
        }
        if(playerNumber == 2) {
            p2Score++;
            P2ScoreText.text = p2Score.ToString();
        }
    }
}
