﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.ObscuredTypes;

public enum BlitzState : int { None = -1, Respawn = 0, Idle, Grab, Pulling, Struggle, Dead }

public class PlayerController : MonoBehaviour {

    // 현재 상태를 저장 할 delegate 형
    private delegate IEnumerator StateDelegator();
    // 현재 상태를 저장 할 delegate 멤버 변수
    private StateDelegator mStateDelegator = null;

    GameObject gameController;

    private Transform pTransform;
    private Animator anim;
    public Transform Blitzcrank;
    public Transform enemyBlitzcrank;

    public BlitzState PlayerState;

    public int PlayerNumber;

    [Range(5, 20)]
    public float rotateVelocity;
    public float clampAngle;

    // Temp Variable
    float tRotZ;

    // About Flash
    [Range(1, 10)]
    public float flashDistance;
    public float flashCoolTime;
    private bool flashEnable;
    public Transform flashEffectPrefab;

    // About Grab
    public Transform GrabPointer;
    public SpriteRenderer GrabPointerSprite;
    private Transform GrabHand, GrabString;
    public float grabCoolTime;
    private bool grabEnable;

    int moveDir;

    public int PlayerDir {
        get { return moveDir; }
    }

    // About Game
    public Transform shieldPrefab;
    public float RespawnTime, InvincibleTime;
    public float BuffDurationTime;
    public GameObject BlueEffect, RedEffect;
    private ObscuredFloat blueDuration, redDuration;

    public float BlueDuration { get { return blueDuration; } }
    public float RedDuration { get { return redDuration; } }

    //UI
    public Image GrabCoolTimer, FlashCoolTimer;
    public Text GrabCoolText, FlashCoolText;

    //Mobile Input
    int leftBtnPointers, rightBtnPointers;

    //IsGamePlay
    private bool play;

    // In Player Vs Computer
    private AiController Com;
    private bool isSoloPlay;
    private ObscuredInt defensiveMatrix = 0;

    public bool IgnoreGrab {
        get {
            return defensiveMatrix != 0;
        }
    }

    public void Initiate(Transform blitzSkin) {
        Transform blitz = Instantiate(blitzSkin, Vector3.zero, Quaternion.identity) as Transform;
        blitz.SetParent(Blitzcrank);
        blitz.localPosition = Vector3.zero;
        blitz.localRotation = Quaternion.identity;

        anim = blitz.GetComponent<Animator>();

        GrabString = blitz.FindChild("Grab_Arm_Right").FindChild("Grab_String");
        GrabHand = blitz.FindChild("Grab_Arm_Right").FindChild("Grab_String").FindChild("Grab_Hand");

        PlayerState = BlitzState.Respawn;
        mStateDelegator = Respawn;
        StartCoroutine(MainLoop());
        
        play = true;
        flashEnable = true; grabEnable = true;
    }

  
    // Use this for initialization
    void Start() {
        pTransform = transform;
        GrabPointer.gameObject.SetActive(false);
        moveDir = 0;
        PlayerState = BlitzState.None;
        flashEnable = false; grabEnable = false;
        play = false;
        gameController = GameObject.FindWithTag("GameController");

        isSoloPlay = SceneManager.GetActiveScene().name == "PlayerVsComputer";
        if(isSoloPlay) {
            Com = GameObject.FindWithTag("Computer").GetComponent<AiController>();

            GameConsts.SkinInfo.BonusEffect totalBonus = GameConsts.TotalBonus;

            BuffDurationTime += totalBonus.BonusBuffDuration;
            defensiveMatrix = totalBonus.BonusShield;
        } else {
            Com = null;
        }
    }

    // Update is called once per frame
    void Update() {
        if(play && PlayerState == BlitzState.Idle) {
            if(GameConsts.PCTEST) {
                KeyboardInput();
            } else {
                ButtonInput();
            }
        }
    }

    // 메인 루프
    IEnumerator MainLoop() {
        // 무한 반복 한다.
        while(true) {
            yield return null;

            // 현재 상태에 따라 해당 코루틴 함수를 실행 시킨다.
            yield return StartCoroutine(mStateDelegator());
        }
    }

    // Respawn Blitzcrank
    IEnumerator Respawn() {
        pTransform.rotation = PlayerNumber == 1 ? Quaternion.identity : Quaternion.AngleAxis(180f, Vector3.forward);
        Blitzcrank.localPosition = new Vector3(0f, 13f, 0f);
        Blitzcrank.localRotation = Quaternion.identity;

        GrabPointer.gameObject.SetActive(true);
        StartCoroutine("TictocPointer", true);
        StartCoroutine(InvincibleUnit());

        anim.SetBool("Respawn", true);
        SoundManager.Instance.PlaySfx(Sfx.Respawn);

        blueDuration = 0f;
        redDuration = 0f;

        //Wait Respawn Animation
        yield return new WaitForSeconds(0.34f);

        //On End Animation
        anim.SetBool("Respawn", false);
        PlayerState = BlitzState.Idle;
        mStateDelegator = Idle;
    }

    IEnumerator InvincibleUnit() {
        //invincible true
        Blitzcrank.GetComponent<CircleCollider2D>().enabled = false;
        Transform shield = Instantiate(shieldPrefab, Blitzcrank.position, Blitzcrank.rotation);
        shield.SetParent(Blitzcrank);

        yield return new WaitForSeconds(InvincibleTime);

        //invincible false
        Destroy(shield.gameObject);
        Blitzcrank.GetComponent<CircleCollider2D>().enabled = true;
    }

    public void DefenseGrab() {
        StartCoroutine(InvincibleUnit());
        defensiveMatrix--;
    }

    // Idle (& Walking)
    IEnumerator Idle() {
        while(PlayerState == BlitzState.Idle) {
            MoveBlitz();
            yield return new WaitForFixedUpdate();
        }
    }

    void MoveBlitz() {
        pTransform.rotation *= Quaternion.AngleAxis(moveDir * rotateVelocity * Time.fixedDeltaTime * (redDuration != 0 ? GameConsts.RateOfBuffVelocity : 1f), Vector3.forward);

        tRotZ = pTransform.eulerAngles.z;
        if(PlayerNumber == 1) {
            if(tRotZ > clampAngle && tRotZ < 180f)
                pTransform.rotation = Quaternion.AngleAxis(clampAngle, Vector3.forward);
            if(tRotZ > 180f && tRotZ < 360f - clampAngle)
                pTransform.rotation = Quaternion.AngleAxis(-clampAngle, Vector3.forward);
        }
        if(PlayerNumber == 2) {
            if(tRotZ > 180f + clampAngle)
                pTransform.rotation = Quaternion.AngleAxis(180f + clampAngle, Vector3.forward);
            if(tRotZ < 180f - clampAngle)
                pTransform.rotation = Quaternion.AngleAxis(180f - clampAngle, Vector3.forward);
        }

        Blitzcrank.localRotation = Quaternion.AngleAxis(moveDir * 50f, Vector3.back);

        if(moveDir != 0) {
            anim.SetBool("Walking", true);
        } else {
            anim.SetBool("Walking", false);
        }
    }

    public void OnFlash() {
        if(flashEnable && (PlayerState == BlitzState.Idle) && play) {
            Blitzcrank.GetComponent<CircleCollider2D>().enabled = false;

            SoundManager.Instance.PlaySfx(Sfx.Flash);

            Instantiate(flashEffectPrefab, Blitzcrank.position, Blitzcrank.rotation);
            StartCoroutine("CoolTime", "Flash");

            pTransform.rotation *= Quaternion.AngleAxis(moveDir * flashDistance, Vector3.forward);
            tRotZ = pTransform.eulerAngles.z;
            if(PlayerNumber == 1) {
                if(tRotZ > clampAngle && tRotZ < 180f)
                    pTransform.rotation = Quaternion.AngleAxis(clampAngle, Vector3.forward);
                if(tRotZ > 180f && tRotZ < 360f - clampAngle)
                    pTransform.rotation = Quaternion.AngleAxis(-clampAngle, Vector3.forward);
            }
            if(PlayerNumber == 2) {
                if(tRotZ > 180f + clampAngle)
                    pTransform.rotation = Quaternion.AngleAxis(180f + clampAngle, Vector3.forward);
                if(tRotZ < 180f - clampAngle)
                    pTransform.rotation = Quaternion.AngleAxis(180f - clampAngle, Vector3.forward);
            }

            Blitzcrank.localRotation = Quaternion.AngleAxis(moveDir * 50f, Vector3.back);

            Blitzcrank.GetComponent<CircleCollider2D>().enabled = true;
        }
    }

    public void OnGrab() {
        if(grabEnable && (PlayerState == BlitzState.Idle) && play) {
            if(isSoloPlay) {
                // Send Grab Destination Position
                Com.OnPlayerGrab(Blitzcrank.position, GrabPointer.up * -1f);
            }

            PlayerState = BlitzState.Grab;
            mStateDelegator = Grab;
            StartCoroutine("CoolTime", "Grab");
        }
    }

    IEnumerator CoolTime(string name) {
        switch(name) {
        case "Flash":
            flashEnable = false;
            FlashCoolText.enabled = true;
            FlashCoolTimer.fillAmount = 1f;
            while(FlashCoolTimer.fillAmount > 0) { 
                FlashCoolText.text = (FlashCoolTimer.fillAmount * flashCoolTime * (blueDuration != 0 ? 1f / GameConsts.RateOfBuffCoolTime : 1f)).ToString("0.0");
                FlashCoolTimer.fillAmount -= (Time.fixedDeltaTime / flashCoolTime) * (blueDuration != 0 ? GameConsts.RateOfBuffCoolTime : 1f);
                yield return new WaitForFixedUpdate();
            }
            FlashCoolTimer.fillAmount = 0f;
            FlashCoolText.enabled = false;
            flashEnable = true;
            break;

        case "Grab":
            grabEnable = false;
            GrabPointerSprite.color = Color.black;
            GrabCoolText.enabled = true;
            GrabCoolTimer.fillAmount = 1f;
            while(GrabCoolTimer.fillAmount > 0) {
                GrabCoolText.text = (GrabCoolTimer.fillAmount * grabCoolTime * (blueDuration != 0 ? 1f / GameConsts.RateOfBuffCoolTime : 1f)).ToString("0.0");
                GrabCoolTimer.fillAmount -= (Time.fixedDeltaTime / grabCoolTime) * (blueDuration != 0 ? GameConsts.RateOfBuffCoolTime : 1f);
                yield return new WaitForFixedUpdate();
            }
            GrabCoolTimer.fillAmount = 0f;
            GrabCoolText.enabled = false;
            GrabPointerSprite.color = new Color32(0, 255, 234, 255);
            grabEnable = true;
            break;
        }
    }

    public void GrabObject(Transform obj) {
        switch(obj.tag) {
        case "Player":
            if(obj != Blitzcrank && PlayerState == BlitzState.Grab) {
                SoundManager.Instance.PlaySfx(Sfx.Catch);
                enemyBlitzcrank = obj;
                PlayerState = BlitzState.Pulling;
                mStateDelegator = Pulling;
            }
            break;

        case "Blue":
            SoundManager.Instance.PlaySfx(Sfx.Catch);
            gameController.SendMessage("GetBuff", "Blue");
            if(isSoloPlay) {
                GameController.GoldEffectInfo goldInfo = new GameController.GoldEffectInfo(GameConsts.BuffGold, obj.position);
                gameController.SendMessage("GetGold", goldInfo);
            }
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            anim.SetBool("Grab", false);
            //GetBlue
            blueDuration = BuffDurationTime;
            StartCoroutine("GetBuff", "Blue");
            Destroy(obj.parent.gameObject);
            break;

        case "Red":
            SoundManager.Instance.PlaySfx(Sfx.Catch);
            gameController.SendMessage("GetBuff", "Red");
            if(isSoloPlay) {
                GameController.GoldEffectInfo goldInfo = new GameController.GoldEffectInfo(GameConsts.BuffGold, obj.position);
                gameController.SendMessage("GetGold", goldInfo);
            }
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            anim.SetBool("Grab", false);
            //GetRed
            redDuration = BuffDurationTime;
            StartCoroutine("GetBuff", "Red");
            Destroy(obj.parent.gameObject);
            break;

        case "Minion":
            SoundManager.Instance.PlaySfx(Sfx.Catch);
            GrabHand.GetComponent<CircleCollider2D>().enabled = false;
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            GrabHand.GetComponent<CircleCollider2D>().enabled = true;
            anim.SetBool("Grab", false);
            //Add Gold
            gameController.SendMessage("GetMinion", new GameController.GoldEffectInfo(0, obj.position));
            Destroy(obj.parent.gameObject);
            break;
        }
    }

    void OnStruggle() {
        anim.SetBool("Grab", false);
        PlayerState = BlitzState.Struggle;
        mStateDelegator = Struggle;
    }

    IEnumerator Struggle() {
        GrabPointer.gameObject.SetActive(false);
        StopCoroutine("TictocPointer");
        anim.SetBool("Struggle", true);
        Blitzcrank.localRotation = Quaternion.identity;
        Blitzcrank.GetComponent<CircleCollider2D>().enabled = false;

        while(PlayerState == BlitzState.Struggle)
            yield return new WaitForFixedUpdate();
    }

    void OnDead() {
        StopCoroutine("GetBuff");
        RedEffect.SetActive(false);
        BlueEffect.SetActive(false);
        blueDuration = 0f;
        redDuration = 0f;
        PlayerState = BlitzState.Dead;
        mStateDelegator = Dead;
    }

    IEnumerator Dead() {
        anim.SetBool("Struggle", false);
        anim.SetBool("Dead", true);
        yield return new WaitForSeconds(RespawnTime);
        anim.SetBool("Dead", false);
        PlayerState = BlitzState.Respawn;
        mStateDelegator = Respawn;
    }

    IEnumerator TictocPointer(bool clockwise) {
        int frameCnt = 30;
        float rotateAngle = 80f;
        float centerAngle = PlayerNumber == 1 ? 0f : 180f;
        if(clockwise)
            GrabPointer.rotation = Quaternion.AngleAxis(centerAngle + rotateAngle / 2f, Vector3.forward);
        else
            GrabPointer.rotation = Quaternion.AngleAxis(centerAngle + rotateAngle / 2f, Vector3.back);

        for(int i = 0; i < frameCnt; i++) {
            if(clockwise) {
                GrabPointer.rotation = Quaternion.AngleAxis(centerAngle + (rotateAngle / 2f) - i * rotateAngle / (float)frameCnt, Vector3.forward);
                yield return new WaitForFixedUpdate();
            } else {
                GrabPointer.rotation = Quaternion.AngleAxis(centerAngle + (rotateAngle / 2f) - i * rotateAngle / (float)frameCnt, Vector3.back);
                yield return new WaitForFixedUpdate();
            }
        }

        StartCoroutine("TictocPointer", !clockwise);
    }

    IEnumerator Grab() {
        anim.SetBool("Grab", true);
        SoundManager.Instance.PlaySfx(Sfx.LaunchGrab);
        Blitzcrank.rotation = GrabPointer.rotation;

        if(Blitzcrank.GetChild(2).name.Contains("Police"))
            SoundManager.Instance.PlaySfx(Sfx.Siren);

        int frameCnt = 20;
        GrabString.localScale = new Vector3(1f, 0f, 1f);
        GrabHand.localScale = Vector3.one;
        for(int i = 0; i < frameCnt && PlayerState == BlitzState.Grab; i++) {
            GrabString.localScale = new Vector3(1f, 1.2f * (i + 1) / (float)frameCnt, 1f);
            GrabHand.localScale = new Vector3(1f, 1f / (1.2f * (i + 1) / (float)frameCnt), 1f);
            yield return new WaitForFixedUpdate();
        }

        if(PlayerState != BlitzState.Pulling && PlayerState != BlitzState.Struggle) {
            PlayerState = BlitzState.Idle;
            mStateDelegator = Idle;
            anim.SetBool("Grab", false);
        }
    }

    IEnumerator Pulling() {
        GrabHand.GetComponent<CircleCollider2D>().enabled = false;
        enemyBlitzcrank.SendMessageUpwards("OnStruggle");

        gameController.SendMessage("GetScore", PlayerNumber);

        float grabStartPoint = GrabString.localScale.y; int frameCnt = (int)(20f * (grabStartPoint - 0.05f) / 1.2f);
        for(int i = 0; i < frameCnt; i++) {
            GrabString.localScale = new Vector3(1f, grabStartPoint - (1.2f * (i + 1) / 20f), 1f);
            GrabHand.localScale = new Vector3(1f, 1f / (grabStartPoint - 1.2f * (i + 1) / 20f), 1f);
            enemyBlitzcrank.position = GrabHand.position;
            yield return new WaitForFixedUpdate();
        }
        
        if(isSoloPlay) {
            if(Com.BlueDuration != 0) {
                blueDuration = Com.BlueDuration;
                StartCoroutine("GetBuff", "Blue");
            }
            if(Com.RedDuration != 0) {
                redDuration = Com.RedDuration;
                StartCoroutine("GetBuff", "Red");
            }
        } else {
            float bd = enemyBlitzcrank.parent.GetComponent<PlayerController>().BlueDuration;
            float rd = enemyBlitzcrank.parent.GetComponent<PlayerController>().RedDuration;
            if(bd != 0) {
                blueDuration = bd;
                StartCoroutine("GetBuff", "Blue");
            }
            if(rd != 0) {
                redDuration = rd;
                StartCoroutine("GetBuff", "Red");
            }
        }

        enemyBlitzcrank.SendMessageUpwards("OnDead");
        GrabHand.GetComponent<CircleCollider2D>().enabled = true;

        anim.SetBool("Grab", false);
        PlayerState = BlitzState.Idle;
        mStateDelegator = Idle;
    }

    IEnumerator GetBuff(string buff) {
        switch(buff) {
        case "Blue":
            BlueEffect.SetActive(true);
            while(blueDuration > 0) {
                blueDuration -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
            blueDuration = 0f;
            BlueEffect.SetActive(false);
            break;

        case "Red":
            RedEffect.SetActive(true);
            while(redDuration > 0) {
                redDuration -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
            redDuration = 0f;
            RedEffect.SetActive(false);
            break;
        }
    }

    public void OnGameEnd() {
        play = false;
    }

    public void OnContinue() {
        play = true;

        leftBtnPointers = 0;
        rightBtnPointers = 0;

        PlayerState = BlitzState.Respawn;
        mStateDelegator = Respawn;
    }

    void KeyboardInput() {
        moveDir = (int)Input.GetAxisRaw("Horizontal" + PlayerNumber.ToString());

        if(Input.GetButtonDown("Fire" + PlayerNumber.ToString())) {
            OnGrab();
        }
        if(Input.GetButtonDown("Flash" + PlayerNumber.ToString())) {
            OnFlash();
        }
    }

    public void ButtonEnter(string btn) {
        switch(btn) {
        case "left":
            leftBtnPointers++;
            break;

        case "right":
            rightBtnPointers++;
            break;
        }
    }

    public void ButtonExit(string btn) {
        switch(btn) {
        case "left":
            leftBtnPointers--;
            if(leftBtnPointers < 0)
                leftBtnPointers = 0;
            break;

        case "right":
            rightBtnPointers--;
            if(rightBtnPointers < 0)
                rightBtnPointers = 0;
            break;
        }
    }

    void ButtonInput() {
        if(leftBtnPointers == 0 && rightBtnPointers == 0) {
            moveDir = 0;
        } else {
            if(leftBtnPointers > 0 && rightBtnPointers == 0) {
                moveDir = -1;
            }
            if(rightBtnPointers > 0 && leftBtnPointers == 0) {
                moveDir = 1;
            }
        }
    }
}
