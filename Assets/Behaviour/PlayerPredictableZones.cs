﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPredictableZones : MonoBehaviour {

    public Transform ifPlayerFlash;
    public Transform ifPlayerMove;

    PlayerController player;

	// Use this for initialization
	void Start () {
        player = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        ifPlayerFlash.localRotation = Quaternion.AngleAxis(10f * player.PlayerDir, Vector3.forward);
        ifPlayerMove.localRotation = Quaternion.AngleAxis(3f * player.PlayerDir, Vector3.forward);
    }
}
