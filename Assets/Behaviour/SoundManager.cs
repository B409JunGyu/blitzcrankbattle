﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sfx : int { Button = 0, Respawn, Flash, LaunchGrab, Catch, GameOver, Siren };
public enum Bgm : int { KingOfBlitzcrank };
public enum BlitzcrankScript : int { Concentrate };
public enum AnnounceScript : int { Welcome = 0, FirstBlood, KillEnemy, DoubleKill, TripleKill, QuadraKill, PentaKill, KillingSpree, Rampage, Unstoppable, Dominating, Godlike, Legendary, Shutdown, KilledByEnemy };

public class SoundManager : MonoBehaviour {

    private static SoundManager instance = null;

    public static SoundManager Instance {
        get {
            if(instance == null)
                instance = (SoundManager)FindObjectOfType(typeof(SoundManager));
            return instance;
        }
    }

    public AudioSource BgmSource;
    public AudioSource SfxSource;
    public AudioSource AnnounceSource;

    public AudioClip[] Bgms;
    public AudioClip[] Sfxs;
    public AudioClip[] Announces_KR;
    public AudioClip[] Announces_EN;

    void Awake() {
        DontDestroyOnLoad(transform.gameObject);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void PlaySfx(Sfx sfx) {
        SfxSource.PlayOneShot(Sfxs[(int)sfx]);
    }

    public void PlayBgm(Bgm bgm) {
        BgmSource.clip = Bgms[(int)bgm];
        BgmSource.Play();
    }

    public void Announce(AnnounceScript script) {
        switch(GameManager.Instance.GameLanguage) {
        case "Korean":
            AnnounceSource.PlayOneShot(Announces_KR[(int)script]);
            break;

        default:
            AnnounceSource.PlayOneShot(Announces_EN[(int)script]);
            break;
        }
    }

    public void SetBgmOn(bool bgmOn) {
        BgmSource.volume = bgmOn ? 1f : 0f;
    }

    public void SetSfxOn(bool sfxOn) {
        SfxSource.volume = sfxOn ? 1f : 0f;
        AnnounceSource.volume = sfxOn ? 1f : 0f;
    }
}
