﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestController : MonoBehaviour {

    public Text DebugText;
    int pNum;

	// Use this for initialization
	void Start () {
        pNum = 0;
        DebugText.text = pNum.ToString();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void PointEnter() {
        pNum++;
        DebugText.text = pNum.ToString();
    }
    
    public void PointExit() {
        pNum--;
        DebugText.text = pNum.ToString();
    }

    public void PointClick() {
        Debug.Log("Click");
    }
}
