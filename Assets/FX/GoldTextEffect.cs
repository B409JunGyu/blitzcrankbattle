﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GoldTextEffect : MonoBehaviour {

    public Sprite[] Number;
    public Sprite plus;

    public SpriteRenderer[] Numbers; // 천의 자리까지
    public Transform goldEffectPrefab;

    public SpriteRenderer GoldText;
    public Sprite[] GoldTextImg; // 0 -> KR, 1 -> EN

	// Use this for initialization
	void Start () {
        switch(GameManager.Instance.GameLanguage) {
        case "Korean":
            GoldText.sprite = GoldTextImg[0];
            break;

        default:
            GoldText.sprite = GoldTextImg[1];
            GoldText.transform.localPosition = new Vector3(0.48f, 0f, 0f);
            break;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowGoldEffect(int gold) {
        StartCoroutine(ShowGoldEffectAnimation(gold));
    }

    IEnumerator ShowGoldEffectAnimation(int gold) {
        int frameCnt = 20;
        int numberLength = gold.ToString().Length;
        for(int i = 0; i < Numbers.Length; i++) {
            if(i > numberLength) {
                Numbers[i].sprite = null;
            } else if(i == numberLength) {
                Numbers[i].sprite = plus;
            } else {
                Numbers[i].sprite = Number[Convert.ToInt32(gold.ToString().Substring(numberLength - 1 - i, 1))];
            }
        }
        Instantiate(goldEffectPrefab, transform.position, Quaternion.identity);
        transform.position += new Vector3(0f, 0.5f, 0f);
        for(int i = 0; i < frameCnt; i++) {
            transform.position += new Vector3(0f, 0.2f / frameCnt, 0f);
            yield return new WaitForFixedUpdate();
        }
        Destroy(gameObject);
    }
}
