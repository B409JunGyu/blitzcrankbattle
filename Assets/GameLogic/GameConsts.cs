﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkinList : int { Basic = 0, CromaBlack, CromaBlue, CromaPurple, CromaRed, Nuke, Nuke_Deathmetal, Piltover, Piltover_Demon, iRobot, iRobot_Megatron, iRobot_Storm, Police, Police_Bulletproof, Chicken2017, Hextech, Captain, Desert, Seal, BOSS, Marine40K, LegendaryBalrog };

public static class GameConsts {
    public const bool PCTEST = false;

    // Play Games plugin debug logs enabled?
    public const bool PlayGamesDebugLogsEnabled = false;

    //public const int SkinNumber = Skins.Length - 1;

    public const float RateOfBuffCoolTime = 2f;
    public const float RateOfBuffVelocity = 2f;

    public const float PlusTime = 5f;

    public static SkinInfo.BonusEffect TotalBonus {
        get {
            int bonusShield = 0;
            float bonusBuffDuration = 0f;
            float bonusPlayTime = 0f;
            float bonusGold = 0f;

            string[] skinUnlock = GameManager.Instance.SkinUnlock;

            for(int i = 0; i < Skins.Length; i++) {
                if(skinUnlock[i] == "True") {
                    bonusShield += Skins[i].SkinEffect.BonusShield;
                    bonusBuffDuration += Skins[i].SkinEffect.BonusBuffDuration;
                    bonusPlayTime += Skins[i].SkinEffect.BonusPlayTime;
                    bonusGold += Skins[i].SkinEffect.BonusGold;
                }
            }
            return new SkinInfo.BonusEffect(bonusShield, bonusBuffDuration, bonusPlayTime, bonusGold);
        }
    }

    public static SkinInfo[] Skins = {
        new SkinInfo("Basic", "기본 스킨", 0, new SkinInfo.BonusEffect(0, 0, 0, 0)),
        new SkinInfo("Croma Black", "크로마 블랙", 10000, new SkinInfo.BonusEffect(0, 0, 0, 2.5f)),
        new SkinInfo("Croma Blue", "크로마 블루", 10000, new SkinInfo.BonusEffect(0, 0, 0, 2.5f)),
        new SkinInfo("Croma Purple", "크로마 퍼플", 10000, new SkinInfo.BonusEffect(0, 0, 0, 2.5f)),
        new SkinInfo("Croma Red", "크로마 레드", 10000, new SkinInfo.BonusEffect(0, 0, 0, 2.5f)),
        new SkinInfo("Boom-Boom", "핵펀치", 25000, new SkinInfo.BonusEffect(0, 0, 0, 5f)),
        new SkinInfo("Death Metal", "데스메탈", 50000, new SkinInfo.BonusEffect(1, 0, 0, 0)),
        new SkinInfo("Piltover", "필트오버", 25000, new SkinInfo.BonusEffect(0, 0, 0, 5f)),
        new SkinInfo("Demon Blitz", "데몬", 50000, new SkinInfo.BonusEffect(1, 0, 0, 0)),
        new SkinInfo("iRobot", "i로봇", 50000, new SkinInfo.BonusEffect(0, 0, 2f, 0)),
        new SkinInfo("Megatron", "메가트론", 75000, new SkinInfo.BonusEffect(0, 1f, 0, 0)),
        new SkinInfo("StormOfHero", "폭풍의 시공", 55555, new SkinInfo.BonusEffect(0, 0, 3f, 0)),
        new SkinInfo("Police Blitz", "경찰 블리츠", 75000, new SkinInfo.BonusEffect(0, 1f, 0, 0)),
        new SkinInfo("Bulletproof", "방탄 블리츠", 75000, new SkinInfo.BonusEffect(1, 0, 0, 0)),
        new SkinInfo("Chicken-Crank", "꼬꼬 블리츠", 2017, new SkinInfo.BonusEffect(0, 0, 0, 0)),
        new SkinInfo("Hextech Blitz", "마법공학 블리츠", 0, new SkinInfo.BonusEffect(2, 0, 0, 0)),
        new SkinInfo("Captain Blitz", "새우잡이 블리츠", 0, new SkinInfo.BonusEffect(0, 0, 0, 10f)),
        new SkinInfo("Desert", "사막", 100000, new SkinInfo.BonusEffect(0, 0, 5f, 0)),
        new SkinInfo("SEAL", "바다", 100000, new SkinInfo.BonusEffect(0, 1f, 0, 0)),
        new SkinInfo("BOSS", "중간보스", 150000, new SkinInfo.BonusEffect(0, 0, 5f, 0)),
        new SkinInfo("Marine40K", "해병40K", 200000, new SkinInfo.BonusEffect(2, 0, 0, 0)),
        new SkinInfo("Legendary Balrog", "레전드 발록", 0, new SkinInfo.BonusEffect(0, 0, 5f, 0))
    };

    public class SkinInfo {
        string SkinName_EN;
        string SkinName_KR;
        public int SkinCost;
        public BonusEffect SkinEffect;

        public string SkinName {
            get {
                switch(GameManager.Instance.GameLanguage) {
                case "Korean":
                    return SkinName_KR;

                default:
                    return SkinName_EN;
                }
            }
        }

        public class BonusEffect {
            public int BonusShield = 0;
            public float BonusBuffDuration = 0f;
            public float BonusPlayTime = 0f;
            public float BonusGold = 0f;

            public BonusEffect(int BonusShield, float BonusBuffDuration, float BonusPlayTime, float BonusGold) {
                this.BonusShield = BonusShield;
                this.BonusBuffDuration = BonusBuffDuration;
                this.BonusPlayTime = BonusPlayTime;
                this.BonusGold = BonusGold;
            }

            public string EffectInfo {
                get {
                    string s = "";
                    if(BonusShield != 0) {
                        s = Strings.BonusShield + " + ";
                        s += BonusShield.ToString();
                    }
                    if(BonusBuffDuration != 0) {
                        s = Strings.BonusBuffDuration + " + ";
                        s += BonusBuffDuration.ToString();
                    }
                    if(BonusPlayTime != 0) {
                        s = Strings.BonusPlayTime + " + ";
                        s += BonusPlayTime.ToString();
                    }
                    if(BonusGold != 0) {
                        s = Strings.BonusGold + " + ";
                        s += BonusGold.ToString();
                        s += "%";
                    }
                    return s;
                }
            }
        }

        public SkinInfo(string SkinName_EN, string SkinName_KR, int SkinCost, BonusEffect SkinEffect) {
            this.SkinName_EN = SkinName_EN;
            this.SkinName_KR = SkinName_KR;
            this.SkinCost = SkinCost;
            this.SkinEffect = SkinEffect;
        }
    }

    public static int[] AchievementUnlockScore = {
        3, 5, 10, 15, 20, 25, 30
    };

    public static int[] MinionGold = {
        10, 20, 30
    };

    public const float AddTime = 30f;

    public const int BuffGold = 20;
    public const int BlitzKillGold = 10;

    public static string SkinInitiate {
        get {
            string s = "True";
            for(int i = 1; i < Skins.Length; i++) {
                s += ",False";
            }
            return s;
        }
    }

    // AiController Consts
    public const float StartMoveDirPersistTime = 1.2f;
    public const float MinMoveDirPersistTime = 0.3f;
    public const float MaxMoveDirPersistTime = 1.3f;
    public const float MoveDirPersistTimeReduceRate = 0.1f;

    public const string RatingURL = "https://play.google.com/store/apps/details?id=com.juniegames.blitzcrankbattle";
}
