﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameIds {
    
    public class Achievement {
       
        public const string Bronze5 = "CgkIjfPk4_8UEAIQAQ";
        public const string BronzeMaster = "CgkIjfPk4_8UEAIQAg";
        public const string Silver = "CgkIjfPk4_8UEAIQAw";
        public const string Gold = "CgkIjfPk4_8UEAIQBA";
        public const string Platinum = "CgkIjfPk4_8UEAIQBQ";
        public const string EloBoosting = "CgkIjfPk4_8UEAIQBg";
        public const string Master = "CgkIjfPk4_8UEAIQBw";

        public static string[] ScoreAchievement = {
            Bronze5,
            BronzeMaster,
            Silver,
            Gold,
            Platinum,
            EloBoosting,
            Master
        };

        public const string CromaRangers = "CgkIjfPk4_8UEAIQCA";
        public const string Nuke = "CgkIjfPk4_8UEAIQCQ";
        public const string DeathMetal = "CgkIjfPk4_8UEAIQCg";
        public const string Piltover = "CgkIjfPk4_8UEAIQCw";
        public const string Demon = "CgkIjfPk4_8UEAIQDA";
        public const string IRobot = "CgkIjfPk4_8UEAIQDQ";
        public const string Megatron = "CgkIjfPk4_8UEAIQDg";
        public const string Storm = "CgkIjfPk4_8UEAIQDw";
        public const string Police = "CgkIjfPk4_8UEAIQEA";
        public const string Bulletproof = "CgkIjfPk4_8UEAIQEQ";
        public const string Chicken2017 = "CgkIjfPk4_8UEAIQFA";
        public const string Hextech = "CgkIjfPk4_8UEAIQFg";
        public const string Captain = "CgkIjfPk4_8UEAIQFQ";
        public const string Desert = "CgkIjfPk4_8UEAIQFw";
        public const string SEAL = "CgkIjfPk4_8UEAIQGA";
        public const string BOSS = "CgkIjfPk4_8UEAIQGQ";
        public const string Marine40K = "CgkIjfPk4_8UEAIQGg";
        public const string LegendaryBalrog = "CgkIjfPk4_8UEAIQGw";
    }

    public readonly static string LeaderboardId = "CgkIjfPk4_8UEAIQEw";
}
