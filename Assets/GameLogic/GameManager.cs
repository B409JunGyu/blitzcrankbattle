﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.Advertisements;
using CodeStage.AntiCheat.ObscuredTypes;

public class GameManager {
    static private GameManager sInstance = new GameManager();

    private SkinList mPlayerSkin = SkinList.Basic;
    private SkinList mPlayer1Skin = SkinList.Basic;
    private SkinList mPlayer2Skin = SkinList.Basic;

    private ObscuredInt mHighScore;
    private ObscuredInt mGold;
    private string[] mSkinUnlock; // 0 -> Basic

    private string mGameLanguage;

    private bool mAuthenticating = false;

    public bool BgmOn = true;
    public bool SfxOn = true;

    public int VideoAdsOrder = 0; // 0 -> Unity Ads, 1 - > Vungle

    private Dictionary<string, bool> mUnlockedAchievements = new Dictionary<string, bool>();

    static public GameManager Instance {
        get {
            return sInstance;
        }
    }

    private GameManager() {
        mHighScore = ObscuredPrefs.GetInt(Strings.PrefsKey_HighScore, 0);
        mGold = ObscuredPrefs.GetInt(Strings.PrefsKey_Gold, 0);

        mSkinUnlock = new string[GameConsts.Skins.Length];
        string[] skinUnlockLocalData = ObscuredPrefs.GetString(Strings.PrefsKey_SkinUnlock, GameConsts.SkinInitiate).Split(new char[] { ',' }); // 0 -> Basic

        for(int i = 0; i < GameConsts.Skins.Length; i++) {
            if(i < skinUnlockLocalData.Length) {
                mSkinUnlock[i] = skinUnlockLocalData[i];
            } else {
                mSkinUnlock[i] = "False";
            }
        }

        SetLanguage(ObscuredPrefs.GetString(Strings.PrefsKey_GameLanguage, "Default"));

        // If Someone Alter Prefs Value
        ObscuredPrefs.onAlterationDetected = OnCheatingDetected;

        if(!GameConsts.PCTEST) {
            Advertisement.Initialize("1239935");
            AppLovin.InitializeSdk();
            AppLovin.PreloadInterstitial();
            Vungle.init("585cacf8a425df684e00005e", "58651db87cb8bdea79000739", "");
        }
    }

    public void SetLanguage(string language) {
        if(language == "Default") {
            switch(Application.systemLanguage) {
            case SystemLanguage.Korean:
                language = "Korean";
                break;

            default:
                language = "English";
                break;
            }
        }
        mGameLanguage = language;
        ObscuredPrefs.SetString(Strings.PrefsKey_GameLanguage, mGameLanguage);
    }

    private void OnCheatingDetected() {
        ObscuredPrefs.SetInt(Strings.PrefsKey_HighScore, 0);
        ObscuredPrefs.SetInt(Strings.PrefsKey_Gold, 0);
        ObscuredPrefs.SetString(Strings.PrefsKey_SkinUnlock, GameConsts.SkinInitiate); // 0 -> Basic

        mHighScore = 0;
        mGold = 0;
        mSkinUnlock = GameConsts.SkinInitiate.Split(new char[] { ',' });
    }
    
    public void ShowInterstitial() {
        if(AppLovin.HasPreloadedInterstitial()) {
            // An ad is currently available, so show the interstitial.
            AppLovin.ShowInterstitial();
        } else {
            // No ad is available.  Perform failover logic...
        }
    }

    public string GameLanguage {
        get {
            return mGameLanguage;
        }
    }

    public int HighScore {
        get {
            return mHighScore;
        }
    }

    public int Gold {
        get {
            return mGold;
        }
    }

    public string[] SkinUnlock {
        get {
            return mSkinUnlock;
        }
    }

    public SkinList PlayerSkin {
        get {
            return mPlayerSkin;
        }
    }

    public SkinList Player1Skin {
        get {
            return mPlayer1Skin;
        }
    }

    public SkinList Player2Skin {
        get {
            return mPlayer2Skin;
        }
    }

    public void Authenticate() {
        if(Authenticated || mAuthenticating) {
            Debug.LogWarning("Ignoring repeated call to Authenticate().");
            return;
        }

        // Enable/disable logs on the PlayGamesPlatform
        PlayGamesPlatform.DebugLogEnabled = GameConsts.PlayGamesDebugLogsEnabled;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .Build();
        PlayGamesPlatform.InitializeInstance(config);

        // Activate the Play Games platform. This will make it the default
        // implementation of Social.Active
        //PlayGamesPlatform.Activate();

        PlayGamesPlatform.Instance.SetDefaultLeaderboardForUI(GameIds.LeaderboardId);

        // Sign in to Google Play Games
        mAuthenticating = true;
        PlayGamesPlatform.Instance.localUser.Authenticate((bool success) => {
            mAuthenticating = false;
            if(success) {
                // if we signed in successfully, load data from cloud

                UnlockScoreAchievement();
                UnlockSkinAchievement();

                Debug.Log("Login successful!");
            } else {
                // no need to show error message (error messages are shown automatically
                // by plugin)
                Debug.LogWarning("Failed to sign in with Google Play Games.");
            }
        });
    }

    public bool Authenticating {
        get {
            return mAuthenticating;
        }
    }

    public bool Authenticated {
        get {
            return PlayGamesPlatform.Instance.localUser.authenticated;
        }
    }

    public void SignOut() {
        PlayGamesPlatform.Instance.SignOut();
    }

    public void ShowLeaderboardUI() {
        if(Authenticated) {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(GameIds.LeaderboardId);
        }
    }

    public void ShowAchievementsUI() {
        if(Authenticated) {
            PlayGamesPlatform.Instance.ShowAchievementsUI();
        }
    }

    public void UnlockScoreAchievement() {
        for(int i = 0; i < GameIds.Achievement.ScoreAchievement.Length; i++) {
            if(mHighScore >= GameConsts.AchievementUnlockScore[i]) {
                UnlockAchievement(GameIds.Achievement.ScoreAchievement[i]);
            }
        }
    }

    public void UnlockAchievement(string achId) {
        if(Authenticated && !mUnlockedAchievements.ContainsKey(achId)) {
            PlayGamesPlatform.Instance.ReportProgress(achId, 100.0f, (bool success) =>
            {
            });
            mUnlockedAchievements[achId] = true;
        }
    }

    public void SetPlayerSkin(int playerNumber, SkinList skin) {
        if(playerNumber == 0) {
            mPlayerSkin = skin;
        }
        if(playerNumber == 1) {
            mPlayer1Skin = skin;
        }
        if(playerNumber == 2) {
            mPlayer2Skin = skin;
        }
    }

    public void SetHighScore(int score) {
        if(score > HighScore) {
            mHighScore = score;
            ObscuredPrefs.SetInt(Strings.PrefsKey_HighScore, mHighScore);
            if(Authenticated) {
                PlayGamesPlatform.Instance.ReportScore(mHighScore, GameIds.LeaderboardId, (bool success) => {
                });
            }
        }
    }

    public int GetGoldByAds() {
        float param = UnityEngine.Random.Range(0f, 100f);
        if(param < 0.8f) {
            return 0;
        } else if(param < 2) {
            return 1000;
        } else if(param < 5) {
            return 2000;
        } else if(param < 25) {
            return 3000;
        } else if(param < 45) {
            return 4000;
        } else if(param < 65) {
            return 5000;
        } else if(param < 90) {
            return 6000;
        } else if(param < 97) {
            return 8000;
        } else {
            return 10000;
        }
    }

    public void AddGold(int gold) {
        mGold += gold;
        ObscuredPrefs.SetInt(Strings.PrefsKey_Gold, mGold);
    }

    public void UseGold(int gold) {
        mGold -= gold;
        ObscuredPrefs.SetInt(Strings.PrefsKey_Gold, mGold);
    }

    public void GetSkin(SkinList skin) {
        if(mSkinUnlock[(int)skin] == "False") {
            mSkinUnlock[(int)skin] = "True";
            UseGold(GameConsts.Skins[(int)skin].SkinCost);
        }
        string s = mSkinUnlock[0];
        for(int i = 1; i < mSkinUnlock.Length; i++) {
            s += "," + mSkinUnlock[i];
        }
        ObscuredPrefs.SetString(Strings.PrefsKey_SkinUnlock, s);
        UnlockSkinAchievement();
    }

    public void UnlockSkinAchievement() {
        string[] skinUnlock = SkinUnlock;

        if(skinUnlock[(int)SkinList.CromaBlack] == "True" && skinUnlock[(int)SkinList.CromaBlue] == "True" && skinUnlock[(int)SkinList.CromaPurple] == "True" && skinUnlock[(int)SkinList.CromaRed] == "True") {
            UnlockAchievement(GameIds.Achievement.CromaRangers);
        }

        if(skinUnlock[(int)SkinList.Nuke] == "True")
            UnlockAchievement(GameIds.Achievement.Nuke);

        if(skinUnlock[(int)SkinList.Nuke_Deathmetal] == "True")
            UnlockAchievement(GameIds.Achievement.DeathMetal);

        if(skinUnlock[(int)SkinList.Piltover] == "True")
            UnlockAchievement(GameIds.Achievement.Piltover);

        if(skinUnlock[(int)SkinList.Piltover_Demon] == "True")
            UnlockAchievement(GameIds.Achievement.Demon);

        if(skinUnlock[(int)SkinList.iRobot] == "True")
            UnlockAchievement(GameIds.Achievement.IRobot);

        if(skinUnlock[(int)SkinList.iRobot_Megatron] == "True")
            UnlockAchievement(GameIds.Achievement.Megatron);

        if(skinUnlock[(int)SkinList.iRobot_Storm] == "True")
            UnlockAchievement(GameIds.Achievement.Storm);

        if(skinUnlock[(int)SkinList.Police] == "True")
            UnlockAchievement(GameIds.Achievement.Police);

        if(skinUnlock[(int)SkinList.Police_Bulletproof] == "True")
            UnlockAchievement(GameIds.Achievement.Bulletproof);

        if(skinUnlock[(int)SkinList.Chicken2017] == "True")
            UnlockAchievement(GameIds.Achievement.Chicken2017);

        if(skinUnlock[(int)SkinList.Hextech] == "True")
            UnlockAchievement(GameIds.Achievement.Hextech);

        if(skinUnlock[(int)SkinList.Captain] == "True")
            UnlockAchievement(GameIds.Achievement.Captain);

        if(skinUnlock[(int)SkinList.Desert] == "True")
            UnlockAchievement(GameIds.Achievement.Desert);

        if(skinUnlock[(int)SkinList.Seal] == "True")
            UnlockAchievement(GameIds.Achievement.SEAL);

        if(skinUnlock[(int)SkinList.BOSS] == "True")
            UnlockAchievement(GameIds.Achievement.BOSS);

        if(skinUnlock[(int)SkinList.Marine40K] == "True")
            UnlockAchievement(GameIds.Achievement.Marine40K);

        if(skinUnlock[(int)SkinList.LegendaryBalrog] == "True")
            UnlockAchievement(GameIds.Achievement.LegendaryBalrog);
    }

    public bool IsAppInstalled(string bundleID) {
#if UNITY_ANDROID
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
        Debug.Log(" ********LaunchOtherApp ");
        AndroidJavaObject launchIntent = null;
        //if the app is installed, no errors. Else, doesn't get past next line
        try {
            launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleID);
            //        
            //        ca.Call("startActivity",launchIntent);
        }
        catch(Exception ex) {
            Debug.Log("exception" + ex.Message);
        }
        if(launchIntent == null)
            return false;
        return true;
#else
         return false;
#endif
    }
}
