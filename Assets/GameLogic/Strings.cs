﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Strings {
    public const string PrefsKey_HighScore = "HighScore_v1";
    public const string PrefsKey_Gold = "Gold_v1";
    public const string PrefsKey_SkinUnlock = "SkinUnlock_v1";
    public const string PrefsKey_GameLanguage = "GameLanguage";

    public const string CaptainApplicationBundleId = "com.Owings.FishingGame";
    public const string CaptainApplicationURL = "https://play.google.com/store/apps/details?id=com.Owings.FishingGame";

    public const string BalrogApplicationBundleId = "com.Entaro3.TTProject";
    public const string BalrogApplicationURL = "https://play.google.com/store/apps/details?id=com.Entaro3.TTProject";

    public static string GetSkin {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "스킨 받기";

            default:
                return "Get Skin";
            }
        }
    }

    public static string GetHextech {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "마법공학 블리츠 획득!!";

            default:
                return "Get Hextech Blitz!!";
            }
        }
    }

    public static string GoldGained {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return " 골드 획득!";

            default:
                return " Gold Gained!";
            }
        }
    }

    public static string BoxMsg {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "무료 골드\n" + "무료 스킨";

            default:
                return "Gold\n" + "Skin";
            }
        }
    }

    public static string HextechBox {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "마법공학 상자";

            default:
                return "Hextech Box";
            }
        }
    }

    public static string ShowAdsMsg {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "마법공학 블리츠가 낮은 확률로 나옵니다!";

            default:
                return "Hextech Blitz is randomly hiding inside the box!";
            }
        }
    }

    public static string Yes {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "확인";

            default:
                return "Watch";
            }
        }
    }

    public static string No {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "취소";

            default:
                return "Nope";
            }
        }
    }

    public static string Owned {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "보유중";

            default:
                return "Owned";
            }
        }
    }

    public static string GetCaptainBlitz {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "다운받고 새우잡이 블리츠 획득!";

            default:
                return "Download and get Captain Blitz!";
            }
        }
    }

    public static string CaptainBlitzDescription {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "<Color=#D5BA78FF><Size=32>친구가 만든\n게임이에요!</Size>\n\n<Size=15>물고기를 죽창으로 찍어내리ㅅ..</Size></Color>";

            default:
                return "<Color=#D5BA78FF><Size=25>Friend made it :)</Size>\n\n<Size=20>Legendary old man</Size>\n\n<Size=15>annihilating all the fish!</Size></Color>";
            }
        }
    }

    public static string GetLegendaryBalrog {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "다운받고 레전드 발록 획득!";

            default:
                return "Download and get Legendary Balrog!";
            }
        }
    }

    public static string LegendaryBalrogDescription {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "<Color=#D5BA78FF><Size=40>와씨\n발록이다!</Size>\n\n<Size=20>어머 이건 꼭 가져야해!</Size></Color>";

            default:
                return "<Color=#D5BA78FF><Size=40>Fearless\nBalrog</Size>\n\n<Size=28>will assist you!</Size></Color>";
            }
        }
    }

    public static string Download {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "다운로드";

            default:
                return "Download";
            }
        }
    }

    public static string Exit {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "나가기";

            default:
                return "Exit";
            }
        }
    }

    public static string BonusShield {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "그랩 방어";

            default:
                return "Defensive Matrix";
            }
        }
    }

    public static string BonusBuffDuration {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "버프 지속 시간";

            default:
                return "Buff Duration";
            }
        }
    }

    public static string BonusPlayTime {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "추가 시간";

            default:
                return "Bonus Time";
            }
        }
    }

    public static string BonusGold {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "추가 골드";

            default:
                return "Bonus Gold";
            }
        }
    }

    public static string TotalBonus {
        get {
            switch(GameManager.Instance.GameLanguage) {
            case "Korean":
                return "누적 효과";

            default:
                return "Total Bonus";
            }
        }
    }
}
