﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class ShopGui : MonoBehaviour {

    public Text GoldText;

    private Button[] BuyButtons = new Button[GameConsts.Skins.Length - 1];

    public Transform skinCardParent;
    public Sprite bought;

    public Image AdsButtonDisable;
    public Button AdsButton;
    public GameObject ResultAdsUI, GetHextechUI;
    public Text GetHextechText;
    public Text ResultGoldText;

    public Image InhouseScreenshot;
    public Text InhouseTitle;
    public Text InhouseDescription;
    public Text DownloadText, ExitText;
    public Sprite CaptainScreenshot, BalrogScreenshot;
    public GameObject InhouseUI;
    private SkinList currentInhouseSkin;

    public Text TotalEffectText;
    public Text BonusShieldText, BonusBuffDurationText, BonusPlayTimeText, BonusGoldText;
    public Text TotalBonusShield, TotalBonusBuffDuration, TotalBonusPlayTime, TotalBonusGold;

    ShowOptions ShowOpt = new ShowOptions();

    public Text BoxMsg, HextechBoxTitle, ShowAdsMsg, AdsYes, AdsNo;

    void Awake() {
        ShowOpt.resultCallback = OnAdsShowResultCallBack;
        Vungle.onAdFinishedEvent += FinishedViewAds;
    }

    void OnAdsShowResultCallBack(ShowResult result) {
        if(result == ShowResult.Finished) {
            RewardAds();
        }
    }

    public void FinishedViewAds(AdFinishedEventArgs args) {
        if(args.IsCompletedView) {
            RewardAds();
        }
    }

    void RewardAds() {
        int gold = GameManager.Instance.GetGoldByAds();
        if(gold == 0) {
            if(GameManager.Instance.SkinUnlock[(int)SkinList.Hextech] == "True") {
                gold = 10000;
                GameManager.Instance.AddGold(gold);
                ResultGoldText.text = gold.ToString() + Strings.GoldGained;
                ResultAdsUI.SetActive(true);
            } else {
                GameManager.Instance.GetSkin(SkinList.Hextech);
                GetHextechUI.SetActive(true);
            }
        } else {
            GameManager.Instance.AddGold(gold);
            ResultGoldText.text = gold.ToString() + Strings.GoldGained;
            ResultAdsUI.SetActive(true);
        }
        SetUI();
    }

    // Use this for initialization
    void Start () {
        SetUI();
        Time.timeScale = 1f;

        for(int i = 1; i < GameConsts.Skins.Length; i++) {
            skinCardParent.GetChild(i - 1).GetChild(0).GetComponent<Text>().text = GameConsts.Skins[i].SkinName;
        }

        BoxMsg.text = Strings.BoxMsg;
        ShowAdsMsg.text = Strings.ShowAdsMsg;
        HextechBoxTitle.text = Strings.HextechBox;
        AdsYes.text = Strings.Yes;
        AdsNo.text = Strings.No;
        DownloadText.text = Strings.Download;
        ExitText.text = Strings.Exit;
        GetHextechText.text = Strings.GetHextech;

        TotalEffectText.text = Strings.TotalBonus;
        BonusShieldText.text = Strings.BonusShield;
        BonusBuffDurationText.text = Strings.BonusBuffDuration;
        BonusPlayTimeText.text = Strings.BonusPlayTime;
        BonusGoldText.text = Strings.BonusGold;
    }

    // Update is called once per frame
    void Update () {

    }

    public void BuySkin(int skinNumber) {
        if(skinNumber == (int)SkinList.Hextech) {
            // Go To Ads
            return;
        }

        if(skinNumber == (int)SkinList.Captain) {
            // Check Application Installed
            if(GameManager.Instance.IsAppInstalled(Strings.CaptainApplicationBundleId)) { // If app is installed already
                GameManager.Instance.GetSkin((SkinList)skinNumber);
                SetUI();
            } else { // if app is not installed
                // Show UI to install app
                SetInhouseUI((SkinList)skinNumber);
                InhouseUI.SetActive(true);
            }
            return;
        }

        if(skinNumber == (int)SkinList.LegendaryBalrog) {
            // Check Application Installed
            if(GameManager.Instance.IsAppInstalled(Strings.BalrogApplicationBundleId)) { // If app is installed already
                GameManager.Instance.GetSkin((SkinList)skinNumber);
                SetUI();
            } else { // if app is not installed
                // Show UI to install app
                SetInhouseUI((SkinList)skinNumber);
                InhouseUI.SetActive(true);
            }
            return;
        }

        if(GameManager.Instance.Gold >= GameConsts.Skins[skinNumber].SkinCost) {
            GameManager.Instance.GetSkin((SkinList)skinNumber);
            SetUI();
        }
    }

    void SetUI() {
        GoldText.text = GameManager.Instance.Gold.ToString();
        string[] skinUnlock = GameManager.Instance.SkinUnlock;

        for(int i = 1; i < GameConsts.Skins.Length; i++) {
            BuyButtons[i - 1] = skinCardParent.GetChild(i - 1).GetChild(2).GetComponent<Button>();
            skinCardParent.GetChild(i - 1).GetChild(3).GetComponent<Text>().text = GameConsts.Skins[i].SkinEffect.EffectInfo;
            if(skinUnlock[i] == "True") {
                BuyButtons[i - 1].image.sprite = bought;
                BuyButtons[i - 1].enabled = false;
                BuyButtons[i - 1].transform.GetChild(0).GetComponent<Text>().text = Strings.Owned;
            } else {
                switch(i) {
                case (int)SkinList.Hextech:
                    BuyButtons[i - 1].transform.GetChild(0).GetComponent<Text>().text = Strings.HextechBox;
                    break;

                case (int)SkinList.Captain:
                case (int)SkinList.LegendaryBalrog:
                    BuyButtons[i - 1].transform.GetChild(0).GetComponent<Text>().text = Strings.GetSkin;
                    break;

                default:
                    BuyButtons[i - 1].transform.GetChild(0).GetComponent<Text>().text = GameConsts.Skins[i].SkinCost.ToString();
                    break;
                }
            }
        }

        GameConsts.SkinInfo.BonusEffect totalEffect = GameConsts.TotalBonus;

        TotalBonusShield.text = totalEffect.BonusShield.ToString();
        TotalBonusBuffDuration.text = "+ " + totalEffect.BonusBuffDuration.ToString();
        TotalBonusPlayTime.text = "+ " + totalEffect.BonusPlayTime.ToString();
        TotalBonusGold.text = "+ " + totalEffect.BonusGold.ToString() + "%";
    }

    void SetInhouseUI(SkinList skin) {
        switch(skin) {
        case SkinList.Captain:
            InhouseDescription.text = Strings.CaptainBlitzDescription;
            InhouseTitle.text = Strings.GetCaptainBlitz;
            InhouseScreenshot.sprite = CaptainScreenshot;
            break;

        case SkinList.LegendaryBalrog:
            InhouseDescription.text = Strings.LegendaryBalrogDescription;
            InhouseTitle.text = Strings.GetLegendaryBalrog;
            InhouseScreenshot.sprite = BalrogScreenshot;
            break;
        }
        currentInhouseSkin = skin;
    }

    public void GoDownload() {
        switch(currentInhouseSkin) {
        case SkinList.Captain:
            Application.OpenURL(Strings.CaptainApplicationURL);
            break;

        case SkinList.LegendaryBalrog:
            Application.OpenURL(Strings.BalrogApplicationURL);
            break;
        }
    }

    public void OnBtnUnityAds() {
        if(Advertisement.IsReady() && GameManager.Instance.VideoAdsOrder == 0) {
            Advertisement.Show(ShowOpt);
            GameManager.Instance.VideoAdsOrder = 1;
        } else {
            Dictionary<string, object> options = new Dictionary<string, object>();
            options["incentivized"] = true;
            options["orientation"] = VungleAdOrientation.AutoRotate;
            Vungle.playAdWithOptions(options);
            GameManager.Instance.VideoAdsOrder = 0;
        }
    }

    public void BackToMain() {
        Vungle.onAdFinishedEvent -= FinishedViewAds;

        SoundManager.Instance.PlaySfx(Sfx.Button);
        SceneManager.LoadScene("Main");
    }
}
