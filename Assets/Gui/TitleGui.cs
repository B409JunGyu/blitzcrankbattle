﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class TitleGui : MonoBehaviour {

    public Text HighScoreText;

    public Button BgmBtn, SfxBtn;
    public Image GoogleBtn;
    public Sprite GoogleLogIn, GoogleLogOut;
    public Button Btn_KR, Btn_EN;

    public Sprite[] TitleImg; // 0 - KR, 1 - EN
    public Image Title;

	// Use this for initialization
	void Start () {
        if(!GameManager.Instance.Authenticated) {
            GameManager.Instance.Authenticate();
        }

        if(!SoundManager.Instance.BgmSource.isPlaying) {
            SoundManager.Instance.PlayBgm(Bgm.KingOfBlitzcrank);
        }

        BgmBtn.interactable = GameManager.Instance.BgmOn;
        SfxBtn.interactable = GameManager.Instance.SfxOn;
        Time.timeScale = 1f;

        HighScoreText.text = GameManager.Instance.HighScore.ToString();
        GameManager.Instance.UnlockScoreAchievement();
        GameManager.Instance.UnlockSkinAchievement();
        SetUI();
    }

    // Update is called once per frame
    void Update () {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
        GoogleBtn.sprite = GameManager.Instance.Authenticated ? GoogleLogOut : GoogleLogIn;
    }

    public void PlayerVsPlayer(){
        SoundManager.Instance.PlaySfx(Sfx.Button);
		SceneManager.LoadScene ("PlayerVsPlayer");
	}

	public void PlayerVsComputer(){
        SoundManager.Instance.PlaySfx(Sfx.Button);
		SceneManager.LoadScene ("PlayerVsComputer");
	}

	public void GoShopping(){
        SoundManager.Instance.PlaySfx(Sfx.Button);
		SceneManager.LoadScene ("Shop");
    }

    public void OnAchievement() {
        if(GameManager.Instance.Authenticated) {
            GameManager.Instance.ShowAchievementsUI();
        } else {
            GameManager.Instance.Authenticate();
        }
    }

    public void OnLeaderboard() {
        GameManager.Instance.ShowLeaderboardUI();
    }

    public void BgmOnOff() {
        GameManager.Instance.BgmOn = !GameManager.Instance.BgmOn;
        BgmBtn.image.color = GameManager.Instance.BgmOn ? Color.white : Color.gray;
        SoundManager.Instance.SetBgmOn(GameManager.Instance.BgmOn);
    }

    public void SfxOnOff() {
        GameManager.Instance.SfxOn = !GameManager.Instance.SfxOn;
        SfxBtn.image.color = GameManager.Instance.SfxOn ? Color.white : Color.gray;
        SoundManager.Instance.SetSfxOn(GameManager.Instance.SfxOn);
    }

    public void OnGoogleButton() {
        if(GameManager.Instance.Authenticated)
            GameManager.Instance.SignOut();
        else
            GameManager.Instance.Authenticate();
    }

    public void SetLanguage(string language) {
        GameManager.Instance.SetLanguage(language);
        SetUI();
    }

    void SetUI() {
        switch(GameManager.Instance.GameLanguage) {
        case "Korean":
            Title.sprite = TitleImg[0];
            break;

        default:
            Title.sprite = TitleImg[1];
            break;
        }

        Btn_KR.interactable = GameManager.Instance.GameLanguage != "Korean";
        Btn_KR.transform.GetChild(0).gameObject.SetActive(!Btn_KR.interactable);
        Btn_EN.interactable = GameManager.Instance.GameLanguage != "English";
        Btn_EN.transform.GetChild(0).gameObject.SetActive(!Btn_EN.interactable);
    }

    public void PlayBtnSound() {
        SoundManager.Instance.PlaySfx(Sfx.Button);
    }
}
